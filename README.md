# VEngine Tempest
Implementacja gry w silniku [Vengine](https://gitlab.com/kacperekjh/vengine) napisana jako zadanie konkursowe na Motorola Science Cup 2023. Gra została napisana przez drużynę Vistaaa ze szkoły ZSTiO w Limanowej.

## Co jest w grze?
#### **Przeciwnicy**:
 - **Flipper** - pojawiaja się przez całą grę.
 - **Tanker** - od etapu 3.
 - **Fuseball** - od etapu 4.
 - **Spiker** - od etapu 2.
 - **Burner** (nowość) - przeciwnik poruszający się po zewnętrznej stronie obręczy. Po zbliżeniu się do gracza ładuje laser, którym następnie przez kilka sekund strzela, poruszając się w kierunku gracza.  Pojawiaja się od etapu 3 na planszach tworzących pętle.
#### 5 różnych map.
#### Nieskończona ilośc etapów o stale rosnącym poziomie trudności.

## Kompilacja
#### Wymagania:
- Kompilator C++ wspierający standard C++20 lub nowszy.
- Biblioteki SDL2, SDL2_ttf oraz SDL2_mixer.
- CMake
