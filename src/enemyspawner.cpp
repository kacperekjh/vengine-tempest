#include "enemyspawner.h"
#include "spawners/flipperspawner.h"
#include "spawners/tankerspawner.h"
#include "spawners/fuseballspawner.h"
#include "spawners/spikerspawner.h"
#include "spawners/burnerspawner.h"

struct Spawners
{
    std::vector<EnemySpawner *> spawners = {};

    ~Spawners()
    {
        for (auto spawner : spawners)
        {
            delete spawner;
        }
    }
};

Spawners spawners = {{
    new FlipperSpawner(),
    new TankerSpawner(),
    new FuseBallSpawner(),
    new SpikerSpawner(),
    new BurnerSpawner()
}};

void EnemySpawner::SpawnRandomEnemy()
{
    std::vector<EnemySpawner *> valid = {};
    for (auto spawner : spawners.spawners)
    {
        if (spawner->CanSpawn())
        {
            valid.push_back(spawner);
        }
    }
    if (valid.size() == 0)
    {
        return;
    }
    valid[Random::GetRandomNumber<size_t>(0u, valid.size())]->Spawn();
}