#include <vengine>
#include "components/gamemanager.h"
#include "components/entitymanager.h"
#include "components/mapmanager.h"
#include "components/ui/fpscounter.h"
#include "components/ui/scoredisplay.h"
#include "components/camerafollower.h"
#include "components/menumanager.h"
#include "models/maps/tunnel1.h"
#include "models/maps/tunnel2.h"
#include "models/maps/tunnel3.h"
#include "models/maps/tunnel4.h"
#include "models/maps/line2.h"
#include "keys.h"
#include "renderorder.h"
#include "particles/spaceparticles.h"

const Pointer<Scene> &CreateMenu()
{
    const auto &menu = ObjectManager::CreateScene("menu");
    ObjectManager::CreateObject(menu)->AddComponent<MenuManager>();
    return menu;
}

const Pointer<Scene> &CreateMain()
{
    const auto &main = ObjectManager::CreateScene("main");

    // camera
    auto follower = ObjectManager::CreateObject(main)->AddComponent<CameraFollower>();

    // managers
    auto manager = ObjectManager::CreateObject(main);
    manager->AddComponent<MapManager>();
    auto gamemanager = manager->AddComponent<GameManager>();
    manager->AddComponent<EntityManager>();
    gamemanager->StartGame({
        tunnel3[0],
        tunnel2[0],
        tunnel1[0],
        line2[0],
        tunnel4[0]
    });

    //
    // Particles
    //
    // red
    SpaceParticles particles = SpaceParticles();
    particles.lineWidthMin = 2;
    particles.lineWidthMax = 2;
    particles.color = {250, 0, 0, 255};
    const auto &emiterObject = ObjectManager::CreateObject(main);
    auto emiter1 = emiterObject->AddComponent<ParticleEmiter>();
    emiter1->visual = particles.GetVisualData();
    emiter1->behavior = particles.GetBehaviorData();
    constexpr float range1 = 6.f;
    emiter1->spawn.getPosition = SpawnBehaviors::RandomBox({-range1, -range1, MapManager::depth - 1.f}, {range1, range1, MapManager::depth + 1.f});
    emiter1->particlesPerSecond = 0.5;
    emiter1->visual.renderPriority = RenderOrder::MapFar;
    emiter1->EmitBurst(10);

    // blue
    auto emiter2 = emiterObject->AddComponent<ParticleEmiter>();
    particles.color = {0, 0, 255, 255};
    emiter2->visual = particles.GetVisualData();
    emiter2->behavior = particles.GetBehaviorData();
    constexpr float range2 = -15.f;
    emiter2->spawn.getPosition = SpawnBehaviors::RandomBox({-range2, -range2, follower->offset.z - 20.f}, {range2, range2, follower->offset.z - 60.f});
    emiter2->particlesPerSecond = 4.f;
    emiter2->visual.renderPriority = RenderOrder::Particles;
    emiter2->EmitBurst(20);

    //
    // UI
    //
    auto container = UIManager::CreateElement<UIContainer>(main->GetUIRoot());
    container->SetDirection(UIDirection::LeftToRight);
    container->SetSize({100_pct, size_auto});

    // fps counter
    auto textRoot = UIManager::CreateElement<UIRoot>(container);
    textRoot->SetSize({15_pct, size_auto});
    auto text = UIManager::CreateElement<UIText>(textRoot);
    text->SetFontSize(18);
    text->SetSize({100_pct, size_auto});
    auto fpsCounter = manager->AddComponent<FPSCounter>();
    fpsCounter->text = text;
    fpsCounter->root = textRoot;

    // score
    text = UIManager::CreateElement<UIText>(container);
    text->SetFontSize(22);
    text->SetSize({70_pct, size_auto});
    text->SetTextAlignment(UIAlignment::Center, UIAlignment::Begin);
    manager->AddComponent<ScoreDisplay>()->text = text;

    return main;
}

void Scenes::CreateScenes()
{
    UIButton::SetPressKey(Keys::Get(Keys::Shoot));
    const auto &menu = CreateMenu();
    CreateMain();
    ObjectManager::GoToScene(menu);
}