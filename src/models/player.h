#ifndef _PLAYER_784FBCB2FC48827D_
#define _PLAYER_784FBCB2FC48827D_

#include <vengine>

const std::vector<Line3D> player = {
	std::vector<Vector3>{
		{0, 0.59, -0.05},
		{0, 0, -0.42},
		{0, -0.59, -0.05},
		{0, -0.325, 0.43},
		{0, -0.75, -0.045},
		{0, 0, -0.67},
		{0, 0.75, -0.045},
		{0, 0.4025, 0.455},
		{0, 0.59, -0.05}}};

#endif