#ifndef _TUNNEL2_A8EDD1AF4DC7F633_
#define _TUNNEL2_A8EDD1AF4DC7F633_

#include <vengine>

const std::vector<Line2D> tunnel2 = {
	std::vector<Vector2>{
		{0.75, -2.25},
		{-0.75, -2.25},
		{-2.25, -2.25},
		{-2.25, -0.75},
		{-2.25, 0.75},
		{-2.25, 2.25},
		{-0.75, 2.25},
		{0.75, 2.25},
		{2.25, 2.25},
		{2.25, 0.75},
		{2.25, -0.75},
		{2.25, -2.25},
		{0.75, -2.25}}};

#endif