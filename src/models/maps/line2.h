#ifndef _LINE2_6F026585ECB97C45_
#define _LINE2_6F026585ECB97C45_

#include <vengine>

const std::vector<Line2D> line2 = {
	std::vector<Vector2>{
		{-3.897114, 0.45},
		{-2.598076, -0.3},
		{-1.299038, -1.05},
		{0, -1.8},
		{1.299038, -1.05},
		{2.598076, -0.3},
		{3.897114, 0.45}}};

#endif