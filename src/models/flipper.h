#ifndef _FLIPPER_D76E6DA413507B0D_
#define _FLIPPER_D76E6DA413507B0D_

#include <vengine>

const std::vector<Line3D> flipper = {
	std::vector<Vector3>{
		{0, 0.75, 0},
		{0, 0.45, -0.4},
		{0, 0.75, -0.9},
		{0, -0.75, 0},
		{0, -0.45, -0.4},
		{0, -0.75, -0.9},
		{0, 0.75, 0}}};

#endif