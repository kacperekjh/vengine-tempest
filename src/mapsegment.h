#ifndef _TEMPEST_COMPONENTS_MAPSEGMENT_
#define _TEMPEST_COMPONENTS_MAPSEGMENT_

#include <vengine>
#include "direction.h"

class MapSegment : public SharedPointer<MapSegment>
{
private:
    Pointer<MapSegment> previous = nullptr;
    Pointer<MapSegment> next = nullptr;
    float playerAngle = 0;
    std::pair<Vector2, Vector2> points = {};

    void CreateLines();
    /// @brief Changes the points of this map segment.
    void SetPoints(Vector2 a, Vector2 b);
    int SearchCW(const Pointer<MapSegment> &target) const;
    int SearchCCW(const Pointer<MapSegment> &target) const;
    MapSegment() = default;

public:
    struct Direction
    {
        std::optional<HorizontalDirection> direction;
        std::optional<unsigned> distance = 0;

        operator bool() const;
        operator HorizontalDirection() const;
        bool operator==(HorizontalDirection direction) const;
        Direction &operator=(HorizontalDirection direction);
    };

    /// @brief Returns an angle perpendicular to the segment's direction.
    float GetAngle() const;
    /// @brief Returns the center point of the segment;
    Vector2 GetCenter() const;
    /// @brief Returns the next segment to this (should go counter-clockwise) or null.
    const Pointer<MapSegment> &GetNext() const;
    /// @brief Returns the previous segment to this (should go clockwise) or null.
    const Pointer<MapSegment> &GetPrevious() const;
    /// @brief Returns the neighbor of this segment in `direction`.
    const Pointer<MapSegment> &GetNeighbor(HorizontalDirection direction) const;
    /// @brief Returns the shortest direction and distance to `target` segment.
    Direction FindDirection(const Pointer<MapSegment> &target) const;
    /// @brief Returns the points of this map segment.
    const std::pair<Vector2, Vector2> &GetPoints() const;
    /// @brief Returns a random neighbor segment or null if there are none.
    const Pointer<MapSegment> &GetRandomNeighbor() const;

    friend class MapManager;
};

#endif