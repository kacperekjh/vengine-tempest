#ifndef _TEMPEST_TRANSITION_
#define _TEMPEST_TRANSITION_

#include <vengine>

namespace Transition
{
    constexpr float flySpeed = 25.f;
    constexpr float flyDuration = 1.f;
    constexpr float (*timingFunction)(float) = TimingFunctions::FadeIn;
};

#endif