#include "mapsegment.h"
#include "../renderorder.h"

void MapSegment::SetPoints(Vector2 a, Vector2 b)
{
    points = {a, b};
    playerAngle = Vector2::Direction(a, b).ToAngle() - 90_deg;
}

int MapSegment::SearchCW(const Pointer<MapSegment> &target) const
{
    if (GetPointer() == target)
    {
        return 0;
    }
    else if (previous)
    {
        int out = previous->SearchCW(target);
        return out == -1 ? -1 : out + 1;
    }
    else
    {
        return -1;
    }
}

int MapSegment::SearchCCW(const Pointer<MapSegment> &target) const
{
    if (GetPointer() == target)
    {
        return 0;
    }
    else if (next)
    {
        int out = next->SearchCCW(target);
        return out == -1 ? -1 : out + 1;
    }
    else
    {
        return -1;
    }
}

MapSegment::Direction::operator bool() const
{
    return (bool)direction;
}

MapSegment::Direction::operator HorizontalDirection() const
{
    return direction.value();
}

bool MapSegment::Direction::operator==(HorizontalDirection direction) const
{
    return Direction::direction == direction;
}

MapSegment::Direction &MapSegment::Direction::operator=(HorizontalDirection direction)
{
    Direction::direction = direction;
    return *this;
}

float MapSegment::GetAngle() const
{
    return playerAngle;
}

Vector2 MapSegment::GetCenter() const
{
    return (points.first + points.second) / 2;
}

const Pointer<MapSegment> &MapSegment::GetNext() const
{
    return next;
}

const Pointer<MapSegment> &MapSegment::GetPrevious() const
{
    return previous;
}

const Pointer<MapSegment> &MapSegment::GetNeighbor(HorizontalDirection direction) const
{
    if (direction == HorizontalDirection::CW)
    {
        return GetPrevious();
    }
    else if (direction == HorizontalDirection::CCW)
    {
        return GetNext();
    }
    else    
    {
        throw std::invalid_argument("Invalid direction.");
    }
}

MapSegment::Direction MapSegment::FindDirection(const Pointer<MapSegment> &target) const
{
    if (!target)
    {
        return {std::nullopt, 0};
    }
    int cw = SearchCW(target);
    int ccw = SearchCCW(target);
    if (cw == -1 && ccw == -1)
    {
        return {std::nullopt, std::nullopt};
    }
    if (cw == -1)
    {
        return {HorizontalDirection::CCW, ccw};
    }
    else if (ccw == -1)
    {
        return {HorizontalDirection::CW, cw};
    }
    else
    {
        return cw > ccw ? Direction{HorizontalDirection::CCW, ccw} : Direction{HorizontalDirection::CW, cw};
    }
}

const std::pair<Vector2, Vector2> &MapSegment::GetPoints() const
{
    return points;
}

const Pointer<MapSegment> &MapSegment::GetRandomNeighbor() const
{
    if (GetPrevious() && GetNext())
    {
        return Random::GetRandomBool() ? GetPrevious() : GetNext();
    }
    else if (GetPrevious())
    {
        return GetPrevious();
    }
    else if (GetNext())
    {
        return GetNext();
    }
    else
    {
        return Pointer<MapSegment>::null;
    }
}