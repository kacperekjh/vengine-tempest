#ifndef _TEMPEST_COMPONENTS_MENUMANAGER_
#define _TEMPEST_COMPONENTS_MENUMANAGER_

#include <vengine>

class MenuManager : public Component, public SceneSingleton<MenuManager>
{
private:
    enum class Page
    {
        Main,
        Settings
    };

    enum class FadingStage
    {
        FadingOut,
        FadingIn,
        NoFading
    };

    enum class EnteringGameStage
    {
        Flying,
        FadingOut,
        NotEntering
    };

    struct Button
    {
        Pointer<UIButton> button;
        Pointer<UIText> text;
    };

    Pointer<UIRoot> mainMenu = nullptr;
    Pointer<UIRoot> settings = nullptr;
    Pointer<GameObject> camera = nullptr;
    Page openPage = Page::Main;
    FadingStage fade = FadingStage::NoFading;
    EnteringGameStage stage = EnteringGameStage::NotEntering;
    float progress = 0.f;
    Pointer<UIRoot> fadingTo = nullptr;
    static constexpr float fadeSpeed = 6.f;
    static constexpr float cameraDistance = 4.f;

    Button CreateButton(const Pointer<UIElement> &parent, std::string_view text, const std::function<void()> &callback, const UISizeExpression &width = 400_px) const;
    void CreateWorld();
    void CreateMainMenu(const Pointer<UIRoot> &root);
    void CreateSettings(const Pointer<UIRoot> &root);
    void GoToPage(Page page);
    void GoToGame();
    void HandleFading();
    void HandleGameEnter();

protected:
    void Awake() override;
    void Update() override;
    void OnSceneLoaded() override;

public:
    ~MenuManager();
};

#endif