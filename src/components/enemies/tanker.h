#ifndef _TEMPEST_COMPONENTS_ENEMIES_TANKER_
#define _TEMPEST_COMPONENTS_ENEMIES_TANKER_

#include "../enemy.h"

class Tanker : public Enemy
{
private:
    static constexpr float speed = 0.15f;
    static constexpr Color color = {255, 120, 250, 255};
    static constexpr float splitSpeed = 5.f;
    static constexpr float shootTimeoutMin = 3.f;
    static constexpr float shootTimeoutMax = 4.f;

    Vector3 rotationOffset = {};
    float splitProgress = 0;
    Pointer<MapSegment> movingFrom = nullptr;
    float shootTimeout = shootTimeoutMin;

    static float EaseOut(float x);
    void SpawnFlipper(const Pointer<MapSegment> &segment) const;
    void SpawnFlippers() const;

protected:
    void Awake() override;
    void Update() override;
    void OnPositionChange(const Vector3 &position, const Vector3 &rotation) override;
    void OnPlayerProjectileHit() override;

public:
    Tanker();
};

#endif