#ifndef _TEMPEST_COMPONENTS_ENEMIES_FUSEBALL_
#define _TEMPEST_COMPONENTS_ENEMIES_FUSEBALL_

#include "../enemy.h"

class FuseBall : public Enemy
{
private:
    enum class State
    {
        MovingVertically,
        MovingHorizontally,
        Chasing
    };

    // looks
    static constexpr float lineWidth = 4.f;
    static constexpr Color sparkColors[] = {{255, 0, 0, 255}, {255, 255, 0, 255}, {0, 255, 0, 255}, {255, 0, 255, 255}, {0, 0, 255, 255}};
    static constexpr size_t sparkCount = sizeof(sparkColors) / sizeof(Color);
    static constexpr float sparkAngleDeviation = 10_deg;
    static constexpr unsigned segmentCount = 5;
    static constexpr float segmentLengthMin = 0.075f;
    static constexpr float segmentLengthMax = 0.1f;
    static constexpr float segmentAngleChange = 40_deg;
    static constexpr float segmentInitialAngle = 20_deg;

    // ai
    static constexpr float verticalSpeed = 0.3f;
    static constexpr float horizontalSpeed = 0.55f;
    static constexpr float chaseSpeed = 3.f;
    static constexpr float moveTimeoutMin = 0.5f;
    static constexpr float moveTimeoutMax = 2.f;
    static constexpr float killRadius = 0.2f;
    State state = State::MovingVertically;
    Pointer<MapSegment> movingToFrom = nullptr;
    float moveTimeout = Random::GetRandomNumber(moveTimeoutMin, moveTimeoutMax);
    float moveProgress = 0;
    bool movingCW = false;

    void CreateSparks();
    void HandleVerticalMovement();
    void HandleHorizontalMovement();
    void HandleMovementAnimation();
    void HandleChase();
    void MoveTo(const Pointer<MapSegment> &segment);

protected:
    void Awake() override;
    void Update() override;
    void OnDestroy() override;
    void OnPositionChange(const Vector3 &position, const Vector3 &rotation) override;
    void OnRenderPriorityChange(int priority);

public:
    FuseBall();
};

#endif