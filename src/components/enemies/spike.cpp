#include "spike.h"
#include "../player.h"

void Spike::Awake()
{
    Enemy::Awake();
    GetRenderer()->SetColor({0, 255, 0, 255});
    GetRenderer()->SetWidth(3.f);
    defeated = true;
    destroyOnGameEnd = false;
}

void Spike::Update()
{
    auto player = Player::GetInstance();
    if (player->GetCurrentSegment() != GetCurrentSegment())
    {
        return;
    }
    if (player->GetVerticalPosition() > 1 - length + killOffset)
    {
        player->SetState(PlayerState::Dead);
    }
}

void Spike::OnPlayerProjectileHit()
{
    float length = Spike::length - hitLengthDecrease;
    if (length <= hitLengthDecrease)
    {
        GetOwner()->Destroy();
    }
    SetLength(length);
    Explode(0.4f);
}

float Spike::GetLength()
{
    return length;
}

void Spike::SetLength(float length)
{
    length = std::clamp(length, 0.f, 1.f);
    Spike::length = length;
    GetTransform()->SetLines({{{0, 0, 0}, {0, 0, -MapManager::depth * length}}});
    GetCollider()->SetWidth(MapManager::absDepth * length * 2);
}