#include "burner.h"
#include "../player.h"
#include "../../models/burner.h"
#include "../../renderorder.h"

void Burner::HandleMovement()
{
    auto player = Player::GetInstance();
    if (player->GetState() != PlayerState::Alive)
    {
        return;
    }
    jumpTimeout -= GameLoop::GetDeltaTime();
    const auto &segment = GetCurrentSegment();
    if (jumpTimeout <= jumpOptionalDelay && player->GetCurrentSegment() == segment)
    {
        const auto &target = segment->GetRandomNeighbor();
        if (target)
        {
            Jump(target);
        }
    }
    else if (jumpTimeout <= 0)
    {
        auto direction = segment->FindDirection(player->GetCurrentSegment());
        if (direction)
        {
            if (direction.distance > 1)
            {
                Jump(segment->GetNeighbor(direction));
            }
            else
            {
                Blast();
            }
            return;
        }
    }
}

void Burner::HandleJump()
{
    progress += GameLoop::GetDeltaTime() * jumpSpeed;
    if (charging)
    {
        progress += GameLoop::GetDeltaTime() / jumpChargeTime;
        if (progress >= 0)
        {
            MakeImmune(true);
            progress = 0.f;
            charging = false;
        }
    }
    else
    {
        if (progress >= 0.5f && !jumpChangedSegment)
        {
            SetCurrentSegment(jumpingTo);
            jumpChangedSegment = true;
        }
        if (progress >= 1)
        {
            MakeImmune(false);
            state = State::MovingVertically;
            progress = 0.f;
            return;
        }
        Vector3 endPos = jumpingTo->GetCenter();
        endPos.z = jumpingFrom.z;
        GetOwner()->position = Vector3::Lerp(jumpingFrom, endPos, TimingFunctions::FadeOut(progress));
    }
}

void Burner::HandleBlast()
{
    if (charging)
    {
        progress += GameLoop::GetDeltaTime() / chargeTime;
        if (progress >= 1)
        {
            charging = false;
            progress = 0.f;
            rayRenderer->SetState(true);
            rayEmiter->SetState(true);
            chargeEmiter->SetState(false);
            UpdateLaser();
            UpdateChargeParticles();
        }
    }
    else
    {
        auto player = Player::GetInstance();
        progress += GameLoop::GetDeltaTime() / attackDuration;
        MoveTowardsPlayer();
        UpdateLaser();
        if (progress >= 1 || player->GetState() != PlayerState::Alive)
        {
            state = State::MovingVertically;
            rayRenderer->SetState(false);
            rayEmiter->SetState(false);
            return;
        }
        if (player->GetCurrentSegment() == GetCurrentSegment() && std::abs(player->GetHorizontalPosition() - GetHorizontalPosition()) <= laserRadius / 2)
        {
            player->SetState(PlayerState::Dead);
        }
    }
}

void Burner::MoveTowardsPlayer()
{
    auto player = Player::GetInstance();
    const auto &segment = GetCurrentSegment();
    auto direction = GetCurrentSegment()->FindDirection(player->GetCurrentSegment());
    float position = GetHorizontalPosition();
    if (!direction)
    {
        return;
    }
    if (direction.distance == 0)
    {
        direction = position > player->GetHorizontalPosition() ? HorizontalDirection::CW : HorizontalDirection::CCW;
    }
    if (direction == HorizontalDirection::CW)
    {
        position -= GameLoop::GetDeltaTime() * horizontalMoveSpeed;
    }
    else if (direction == HorizontalDirection::CCW)
    {
        position += GameLoop::GetDeltaTime() * horizontalMoveSpeed;
    }
    while (position > 1)
    {
        if (segment->GetNext())
        {
            SetCurrentSegment(segment->GetNext());
        }
        position--;
    }
    while (position < 0)
    {
        if (segment->GetPrevious())
        {
            SetCurrentSegment(segment->GetPrevious());
        }
        position++;
    }
    SetHorizontalPosition(position);
}

void Burner::Jump(const Pointer<MapSegment> &target)
{   
    jumpingFrom = GetOwner()->position;
    jumpingTo = target;
    state = State::Jumping;
    progress = 0.f;
    jumpChangedSegment = false;
    jumpTimeout = jumpInterval;
    charging = true;
    SetHorizontalPosition(0.5f);
}

void Burner::Blast()
{
    progress = 0.f;
    state = State::Blasting;
    charging = true;
    chargeEmiter->SetState(true);
    UpdateChargeParticles();
}

void Burner::UpdateLaser()
{
    auto camera = Camera::GetInstance();
    if (!camera)
    {
        return;
    }
    // line
    Vector3 from = GetOwner()->position;
    Vector3 to = from;
    to.z = camera->GetOwner()->position.z;
    rayTransform->SetLines({{from, to}});

    // particles
    float length = to.z - from.z;
    rayEmiter->particlesPerSecond = std::abs(length) * rayParticlesPerUnitLength;
    rayEmiter->spawn.getPosition = [from, length](Vector3)
    {
        Vector3 position = Vector2::FromAngle(Random::GetRandomNumber(0_deg, 360_deg)) * laserRadius;
        float t = TimingFunctions::FadeOut<4>(Random::GetRandomNumber(0.f, 1.f)); // spawn more particles closer to the camera
        position.z = std::lerp(0.f, length, t);
        return position + from;
    };
    rayEmiter->spawn.getRotation = [from](Vector3 position)
    {
        Vector3 center = from;
        center.z = 0;
        position -= from;
        position.z = 0;
        Vector2 angles = Vector3::Direction(center, position).ToAngles();
        angles += Vector2(Random::GetRandomNumber(-rayParticlesAngle, rayParticlesAngle), Random::GetRandomNumber(-rayParticlesAngle, rayParticlesAngle));
        return angles;
    };
}

void Burner::UpdateChargeParticles()
{
    Vector3 endPos = GetOwner()->GetGlobalPosition();
    chargeEmiter->spawn.getRotation = [endPos](Vector3 position)
    {
        return Vector3::Direction(position, endPos).ToAngles();
    };
    chargeEmiter->behavior.positionBehavior = [](Vector3 direction, float, float time, float maxTime)
    {
        float t = TimingFunctions::FadeOut(time / maxTime);
        return Particle::MoveData{false, Vector3::Lerp({}, direction * chargeParticleRadius, t)};
    };
}

void Burner::Awake()
{
    Enemy::Awake();
    // ray
    const auto &object = ObjectManager::CreateObject(GetScene());
    GetTransform()->SetLines(blaster);
    GetRenderer()->SetColor({200, 0, 50, 255});
    rayTransform = object->AddComponent<LineTransform>();
    rayRenderer = object->AddComponent<LineRenderer>();
    rayRenderer->SetColor(rayColor);
    rayRenderer->SetWidth(rayLineWidth);
    rayRenderer->SetAntialias(false);
    rayRenderer->SetState(false);
    rayRenderer->SetPriority(RenderOrder::Particles);
    GetOwner()->rotation.z = Random::GetRandomNumber(0_deg, 360_deg);

    //
    // Particles
    //
    // ray
    constexpr float rayHLength = rayParticleLength / 2;
    rayEmiter = object->AddComponent<ParticleEmiter>();
    rayEmiter->visual.lines = {{{{-rayHLength, 0, 0}, {rayHLength, 0, 0}}}};
    rayEmiter->visual.lineWidthMin = rayParticleWidthMin;
    rayEmiter->visual.lineWidthMax = rayParticleWidthMax;
    constexpr Color rayColorTransparent = {particleColor.r, particleColor.b, particleColor.g, 0};
    rayEmiter->visual.colorBehavior = ColorBehaviors::Transition(particleColor, rayColorTransparent, TimingFunctions::FadeOut);
    rayEmiter->visual.renderPriority = RenderOrder::Particles;
    rayEmiter->SetState(false);
    rayEmiter->behavior.timeMin = rayParticlesTimeMin;
    rayEmiter->behavior.timeMax = rayParticlesTimeMax;
    rayEmiter->behavior.speedMin = rayParticlesSpeedMin;
    rayEmiter->behavior.speedMax = rayParticlesSpeedMax;

    // charge
    constexpr float chargeHLength = chargeParticleLength / 2;
    chargeEmiter = GetOwner()->AddComponent<ParticleEmiter>();
    chargeEmiter->visual.lines = {{{{-chargeHLength, 0, 0,}, {chargeHLength, 0, 0}}}};
    chargeEmiter->visual.lineWidthMin = chargeParticleWidthMin;
    chargeEmiter->visual.lineWidthMax = chargeParticleWidthMax;
    constexpr Color chargeColorTransparent = {chargeColor.r, chargeColor.g, chargeColor.b, 0};
    chargeEmiter->visual.colorBehavior = ColorBehaviors::Transition(Gradient<Color>({
        {0.f, chargeColorTransparent},
        {0.2f, chargeColor},
        {1.f, chargeColor}
    }));
    chargeEmiter->behavior.timeMin = chargeParticleTimeMin;
    chargeEmiter->behavior.timeMax = chargeParticleTimeMax;
    chargeEmiter->particlesPerSecond = 15.f;
    chargeEmiter->spawn.getPosition = [](Vector3 position)
    {
        Vector3 offset = Vector3::FromAngles({Random::GetRandomNumber(0_deg, 360_deg), Random::GetRandomNumber(0_deg, 360_deg)});
        offset *= chargeParticleRadius;
        return position + offset;
    };
    chargeEmiter->SetState(false);
}

void Burner::Update()
{
    switch (state)
    {
    case State::MovingVertically:
        HandleMovement();
        break;
    case State::Jumping:
        HandleJump();
        break;
    case State::Blasting:
        HandleBlast();
        break;
    default:
        throw std::runtime_error("Invalid state.");
    }
}

void Burner::OnDestroy()
{
    rayTransform->GetOwner()->Destroy();
}

void Burner::OnPositionChange(const Vector3 &position, const Vector3 &rotation)
{
    if (state != State::Jumping)
    {
        GetOwner()->position = position;
    }
}

Burner::Burner() : Enemy(150) {}