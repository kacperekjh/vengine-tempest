#include "tanker.h"
#include "../models/tanker.h"
#include "../player.h"
#include "flipper.h"
#include <cassert>

float Tanker::EaseOut(float x)
{
    return 1 - std::pow(1 - x, 3);
}

void Tanker::SpawnFlipper(const Pointer<MapSegment> &segment) const
{
    auto object = ObjectManager::CreateObject(GetScene());
    auto flipper = object->AddComponent<Flipper>();
    flipper->SetCurrentSegment(GetCurrentSegment());
    flipper->SetVerticalPosition(GetVerticalPosition());
    flipper->TryMoveTo(segment);
}

void Tanker::SpawnFlippers() const
{
    auto segment = GetCurrentSegment();
    if (segment->GetNext() && segment->GetPrevious())
    {
        SpawnFlipper(segment->GetNext());
        SpawnFlipper(segment->GetPrevious());
    }
    else if (segment->GetNext())
    {
        SpawnFlipper(segment->GetNext());
        SpawnFlipper(segment);
    }
    else if (segment->GetPrevious())
    {
        SpawnFlipper(segment->GetPrevious());
        SpawnFlipper(segment);
    }
    else
    {
        SpawnFlipper(segment);
    }
}

void Tanker::Awake()
{
    Enemy::Awake();
    GetTransform()->SetLines(tanker);
    GetRenderer()->SetColor(color);
    rotationOffset = {Random::GetRandomNumber(0_deg, 360_deg), Random::GetRandomNumber(0_deg, 360_deg), Random::GetRandomNumber(0_deg, 360_deg)};
    GetCollider()->SetWidth(0.6f);
    GetCollider()->RegisterOnCollide([&](const Pointer<Collider> &col)
    {
        if (col->GetEntity()->GetEntityType() == EntityType::Player)
        {
            assert(col->GetEntity().DynamicCast<Player>().GetState());
            col->GetEntity().StaticCast<Player>()->SetState(PlayerState::Dead);
        }
    });
}

void Tanker::Update()
{
    SetVerticalPosition(GetVerticalPosition() - speed * GameLoop::GetDeltaTime());
    if (GetVerticalPosition() <= 0)
    {
        SpawnFlippers();
        GetOwner()->Destroy();
        return;
    }
    if (movingFrom)
    {
        splitProgress += splitSpeed * GameLoop::GetDeltaTime();
        if (splitProgress >= 1)
        {
            movingFrom = nullptr;
            splitProgress = 1;
        }
    }
    shootTimeout -= GameLoop::GetDeltaTime();
    if (shootTimeout <= 0)
    {
        Shoot();
        shootTimeout = Random::GetRandomNumber(shootTimeoutMin, shootTimeoutMax);
    }
}

void Tanker::OnPositionChange(const Vector3 &position, const Vector3 &rotation)
{
    const auto &owner = GetOwner();
    if (movingFrom)
    {
        float t = EaseOut(splitProgress);
        owner->position.x = std::lerp(movingFrom->GetCenter().x, position.x, t);
        owner->position.y = std::lerp(movingFrom->GetCenter().y, position.y, t);
        owner->position.z = position.z;
    }
    else
    {
        owner->position = position;
    }
    owner->rotation = rotation + rotationOffset;
}

void Tanker::OnPlayerProjectileHit()
{
    SpawnFlippers();
    GetOwner()->Destroy();
}

Tanker::Tanker() : Enemy(50) {}