#ifndef _TEMPEST_COMPONENTS_ENEMIES_SPIKE_
#define _TEMPEST_COMPONENTS_ENEMIES_SPIKE_

#include "../enemy.h"

class Spike : public Enemy
{
private:
    float length = 0.f;

protected:
    void Awake() override;
    void Update() override;
    void OnPlayerProjectileHit() override;

public:
    static constexpr float hitLengthDecrease = 0.05f;
    static constexpr float killOffset = 0.4f / MapManager::absDepth;

    float GetLength();

    void SetLength(float length);
};

#endif