#ifndef _TEMPEST_COMPONENTS_ENEMIES_FLIPPER_
#define _TEMPEST_COMPONENTS_ENEMIES_FLIPPER_

#include "../enemy.h"

class Flipper : public Enemy
{
private:
    enum class State
    {
        MovingVertically,
        MovingHorizontally,
        WaitingForDeath,
        Retreating
    }; 

    static constexpr float verticalMoveSpeed = 0.18f;
    static constexpr float horizontalMoveTimeoutMin = 0.35f;
    static constexpr float horizontalMoveTimeoutMax = 0.5f;
    static constexpr float retreatSpeed = 1.2f;
    static constexpr float animationSpeedDeg = 500_deg;
    static constexpr float deathDelay = 0.5f;
    static constexpr float shootTimeoutMin = 1.f;
    static constexpr float shootTimeoutMax = 2.f;
    static constexpr float maxVurnerableAngle = 30_deg;
    float timeout = 0; // horizontal move timeout, death timeout
    float shootTimeout = shootTimeoutMin;
    State state = State::MovingVertically;
    bool animate = false;
    bool movingCW = false;
    float animationStart = 0;
    float animationAngle = 0;
    float animationRemaining = 0;
    bool hasPlayer = false;
    Pointer<MapSegment> movingFrom = nullptr;

    void HandleVerticalMovement();
    void HandleHorizontalMovement();
    void HandleDeathWait();
    void HandleRetreat();
    void HandleAnimation();
    void SetAnimationPosition();
    void StartAnimation();
    void EndAnimation();
    void MoveTo(const Pointer<MapSegment> &segment);
    bool CanMove(const Pointer<MapSegment> &segment);

protected:
    void Awake() override;
    void Update() override;
    void OnDestroy() override;
    void OnPositionChange(const Vector3 &position, const Vector3 &rotation) override;
    void OnPlayerProjectileHit() override;

public:
    static constexpr Color color = {255, 0, 0, 255};

    Flipper();
    bool TryMoveTo(const Pointer<MapSegment> &segment);
};

#endif