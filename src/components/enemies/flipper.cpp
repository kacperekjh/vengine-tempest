#include "flipper.h"
#include "../../models/flipper.h"
#include "../player.h"
#include <stdexcept>
#include "../entitymanager.h"
#include <cassert>

void Flipper::HandleVerticalMovement()
{
    float position = GetVerticalPosition() - verticalMoveSpeed * GameLoop::GetDeltaTime();
    if (position <= 0)
    {
        state = State::MovingHorizontally;
        defeated = true;
    }
    SetVerticalPosition(position);
    if (!animate)
    {
        shootTimeout -= GameLoop::GetDeltaTime();
        if (shootTimeout <= 0)
        {
            Shoot();
            shootTimeout = Random::GetRandomNumber(shootTimeoutMin, shootTimeoutMax);
        }
        timeout -= GameLoop::GetDeltaTime();
        if (timeout <= 0)
        {
            const auto &previous = GetCurrentSegment()->GetPrevious();
            const auto &next = GetCurrentSegment()->GetNext();
            if (CanMove(previous) && CanMove(next))
            {
                MoveTo(Random::GetRandomBool() ? previous : next);
            }
            else if (CanMove(previous))
            {
                MoveTo(previous);
            }
            else if (CanMove(next))
            {
                MoveTo(next);
            }
            else
            {
                return;
            }
            timeout = Random::GetRandomNumber(horizontalMoveTimeoutMin, horizontalMoveTimeoutMax);
        }
    }
}

void Flipper::HandleHorizontalMovement()
{
    const auto &player = Player::GetInstance();
    if (animate || player->GetState() != PlayerState::Alive)
    {
        return;
    }
    if (player->GetCurrentSegment() == GetCurrentSegment())
    {
        player->SetState(PlayerState::Dying1);
        state = State::WaitingForDeath;
        timeout = deathDelay;
        return;
    }
    auto direction = GetCurrentSegment()->FindDirection(player->GetCurrentSegment());
    if (direction)
    {
        TryMoveTo(GetCurrentSegment()->GetNeighbor(direction));
    }
}

void Flipper::HandleDeathWait()
{
    auto player = Player::GetInstance();
    timeout -= GameLoop::GetDeltaTime();
    if (timeout <= 0)
    {
        state = State::Retreating;
        hasPlayer = true;
        player->SetState(PlayerState::Dying2);
    }
}

void Flipper::HandleRetreat()
{
    float position = GetVerticalPosition() + retreatSpeed * GameLoop::GetDeltaTime();
    if (hasPlayer)
    {
        auto player = Player::GetInstance();
        if (position >= 1)
        {
            player->SetState(PlayerState::Dead);
        }
        player->SetVerticalPosition(position);
    }
    SetVerticalPosition(position);
    if (position >= 1)
    {
        GetOwner()->Destroy();
    }
}

void Flipper::HandleAnimation()
{
    if (animate)
    {
        animationAngle += (movingCW ? animationSpeedDeg : -animationSpeedDeg) * GameLoop::GetDeltaTime();
        animationRemaining -= animationSpeedDeg * GameLoop::GetDeltaTime();
        MakeImmune(std::abs(animationAngle) > maxVurnerableAngle && animationRemaining < maxVurnerableAngle);
        if (animationRemaining <= 0)
        {
            EndAnimation();
        }
        else
        {
            SetAnimationPosition();
        }
    }
}

void Flipper::SetAnimationPosition()
{
    Vector3 pos;
    if (movingCW)
    {
        pos = movingFrom->GetPoints().second;
    }
    else
    {
        pos = movingFrom->GetPoints().first;
    }
    pos.z = GetVerticalPosition() * MapManager::depth;
    GetOwner()->position = pos;
    GetOwner()->rotation = {0, 0, animationStart + animationAngle};
}

void Flipper::StartAnimation()
{
    const auto &to= GetCurrentSegment();
    movingCW = to == movingFrom->GetNext();
    animationStart = movingFrom->GetAngle() + 180_deg;
    const auto &points1 = to->GetPoints();
    const auto &points2 = movingFrom->GetPoints();
    Vector2 dir1 = (points1.second - points1.first).Normalized();
    Vector2 dir2 = (points2.second - points2.first).Normalized();
    animationRemaining = 180_deg + atan2(dir1.x * dir2.y - dir1.y * dir2.x, dir1.x * dir2.x + dir1.y * dir2.y);
    if (movingCW)
    {
        animationRemaining = 360_deg - animationRemaining;
        GetTransform()->SetPivotPoint({0, -MapManager::length / 2, 0});
    }
    else
    {
        GetTransform()->SetPivotPoint({0, MapManager::length / 2, 0});
    }
    animationAngle = 0;
    animate = true;
    MakeImmune(true);
}

void Flipper::EndAnimation()
{
    animate = false;
    GetTransform()->SetPivotPoint({});
    UpdatePosition();
    MakeImmune(false);
}

void Flipper::MoveTo(const Pointer<MapSegment> &segment)
{
    movingFrom = GetCurrentSegment();
    SetCurrentSegment(segment);
    StartAnimation();
}

bool Flipper::CanMove(const Pointer<MapSegment> &segment)
{
    if (!segment)
    {
        return false;
    }
    auto collisions = GetCollider()->FindCollisions(segment, GetVerticalPosition(), {EntityType::Enemy});
    return collisions.size() == 0;
}

void Flipper::Awake()
{
    Enemy::Awake();
    GetTransform()->SetLines(flipper);
    GetRenderer()->SetColor(color);
    GetCollider()->SetWidth(0.9f);
    SetVerticalPosition(1);
}

void Flipper::Update()
{
    if (Player::GetInstance()->GetState() != PlayerState::Transitioning)
    {
        if (state != State::Retreating && Player::GetInstance()->GetState() > PlayerState::Dying1)
        {
            state = State::Retreating;
        }
        switch (state)
        {
        case State::MovingVertically:
            HandleVerticalMovement();
            break;
        case State::MovingHorizontally:
            HandleHorizontalMovement();
            break;
        case State::WaitingForDeath:
            HandleDeathWait();
            break;
        case State::Retreating:
            HandleRetreat();
            break;
        default:
            throw std::runtime_error("Invalid Flipper state.");
        }
    }
    HandleAnimation();
}

void Flipper::OnDestroy()
{
    if (hasPlayer)
    {
        Player::GetInstance()->SetState(PlayerState::Dead);
    }
}

void Flipper::OnPositionChange(const Vector3 &position, const Vector3 &rotation)
{
    if (!animate)
    {
        Entity::OnPositionChange(position, rotation);
    }
}

void Flipper::OnPlayerProjectileHit()
{
    if (!hasPlayer)
    {
        Enemy::OnPlayerProjectileHit();
    }
}

Flipper::Flipper() : Enemy(100) {}

bool Flipper::TryMoveTo(const Pointer<MapSegment> &segment)
{
    if (!CanMove(segment))
    {
        return false;
    }
    MoveTo(segment);
    return true;
}