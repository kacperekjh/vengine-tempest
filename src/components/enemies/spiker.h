#ifndef _TEMPEST_COMPONENTS_ENEMIES_SPIKER_
#define _TEMPEST_COMPONENTS_ENEMIES_SPIKER_

#include "../enemy.h"
#include "spike.h"

class Spiker : public Enemy
{
private:
    bool retreating = false;
    Pointer<Spike> spike = nullptr;

protected:
    void Awake() override;
    void Update() override;
    void OnSegmentChange(const Pointer<MapSegment> &previous, const Pointer<MapSegment> &current) override;

public:
    static constexpr float moveSpeed = 0.2f;
    static constexpr float spikeExtension = 0.01f;

    Spiker();
};

#endif