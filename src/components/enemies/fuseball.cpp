#include "fuseball.h"
#include "../player.h"

void FuseBall::CreateSparks()
{
    float angle = Random::GetRandomNumber(0_deg, 360_deg);
    for (const Color color : sparkColors)
    {
        const auto &object = ObjectManager::CreateObject(GetScene());
        object->rotation = {0,0, angle + Random::GetRandomNumber(-sparkAngleDeviation, sparkAngleDeviation)};
        object->SetParent(GetOwner());
        auto transform = object->AddComponent<LineTransform>();
        float segmentAngle = segmentInitialAngle;
        std::vector<Vector3> points = {{}};
        Vector3 previous = {};
        for (unsigned i = 0; i < segmentCount; i++)
        {
            float length = Random::GetRandomNumber(segmentLengthMin, segmentLengthMax);
            segmentAngle += Random::GetRandomNumber(-FuseBall::segmentAngleChange, FuseBall::segmentAngleChange);
            Vector2 dir = Vector2::FromAngle(segmentAngle) * length;
            previous += dir;
            points.push_back(previous);
            segmentAngle *= -1;
        }
        transform->SetLines(Line3D{points});
        auto renderer = object->AddComponent<LineRenderer>();
        renderer->SetColor(color);
        renderer->SetWidth(lineWidth);
        renderer->SetAntialias(false);
        angle += 360_deg / sparkCount;
    }
}

void FuseBall::HandleVerticalMovement()
{
    float position = GetVerticalPosition() - verticalSpeed * GameLoop::GetDeltaTime();
    if (position <= 0)
    {
        state = State::Chasing;
        position = 0;
        SetVerticalPosition(0);
        defeated = true;
        return;
    }
    SetVerticalPosition(position);
    moveTimeout -= GameLoop::GetDeltaTime();
    if (moveTimeout <= 0)
    {
        const auto &segment = GetCurrentSegment()->GetRandomNeighbor();
        if (segment)
        {
            MoveTo(segment);
        }
        moveTimeout = Random::GetRandomNumber(moveTimeoutMin, moveTimeoutMax);
    }
}

void FuseBall::HandleHorizontalMovement()
{
    moveProgress += horizontalSpeed * GameLoop::GetDeltaTime();
    if (moveProgress >= 1)
    {
        moveProgress = 1;
        state = State::MovingVertically;
        MakeImmune(true);
        if (!movingCW)
        {
            SetCurrentSegment(movingToFrom);
        }
    }
    HandleMovementAnimation();
}

void FuseBall::HandleMovementAnimation()
{
    Vector2 a, b;
    if (movingCW)
    {
        a = movingToFrom->GetPoints().first;
        b = GetCurrentSegment()->GetPoints().first;
    }
    else
    {
        a = GetCurrentSegment()->GetPoints().first;
        b = movingToFrom->GetPoints().first;
    }
    Vector2 position = Vector2::Lerp(a, b, moveProgress);
    GetOwner()->position = Vector3(position.x, position.y, GetVerticalPosition() * MapManager::depth);
}

void FuseBall::HandleChase()
{
    auto player = Player::GetInstance();
    if (player->GetState() != PlayerState::Alive)
    {
        return;
    }
    const auto &segment = GetCurrentSegment();
    float position = GetHorizontalPosition();
    if (segment == player->GetCurrentSegment())
    {
        float playerPosition = player->GetHorizontalPosition();
        if (std::abs(position - playerPosition) <= killRadius)
        {
            player->SetState(PlayerState::Dead);
            return;
        }
        if (position > playerPosition)
        {
            position -= chaseSpeed * GameLoop::GetDeltaTime();
        }
        else
        {
            position += chaseSpeed * GameLoop::GetDeltaTime();
        }
    }
    else
    {
        auto direction = GetCurrentSegment()->FindDirection(player->GetCurrentSegment());
        if (direction == HorizontalDirection::CW)
        {
            position -= chaseSpeed * GameLoop::GetDeltaTime();
        }
        else if (direction == HorizontalDirection::CCW)
        {
            position += chaseSpeed * GameLoop::GetDeltaTime();
        }
        while (position > 1)
        {
            SetCurrentSegment(segment->GetNext());
            position--;
        }
        while (position < 0)
        {
            SetCurrentSegment(segment->GetPrevious());
            position++;
        }
    }
    SetHorizontalPosition(position);
}

void FuseBall::MoveTo(const Pointer<MapSegment> &segment)
{
    state = State::MovingHorizontally;
    moveProgress = 0;
    MakeImmune(false);
    movingCW = segment == GetCurrentSegment()->GetPrevious();
    if (movingCW)
    {
        movingToFrom = GetCurrentSegment();
        SetCurrentSegment(segment);
    }
    else
    {
        movingToFrom = segment;
    }
}

void FuseBall::Awake()
{
    Enemy::Awake();
    CreateSparks();
    MakeImmune(true);
    SetHorizontalPosition(0);
}

void FuseBall::Update()
{
    if (Player::GetInstance()->GetState() == PlayerState::Transitioning)
    {
        return;
    }
    switch (state)
    {
    case State::MovingVertically:
        HandleVerticalMovement();
        break;
    case State::MovingHorizontally:
        HandleHorizontalMovement();
        break;
    case State::Chasing:
        HandleChase();
        break;
    }
}

void FuseBall::OnDestroy()
{
    for (const auto &child : GetOwner()->GetChildren())
    {
        child->Destroy();
    }
}

void FuseBall::OnPositionChange(const Vector3 &position, const Vector3 &rotation)
{
    if (state == State::MovingHorizontally)
    {
        HandleMovementAnimation();
    }
    else
    {
        Enemy::OnPositionChange(position, {});
    }
}

void FuseBall::OnRenderPriorityChange(int priority)
{
    for (const auto &child : GetOwner()->GetChildren())
    {
        child->GetComponent<LineRenderer>()->SetPriority(priority);
    }
}

FuseBall::FuseBall() : Enemy(100) {}