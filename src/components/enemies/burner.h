#ifndef _TEMPEST_COMPONENTS_ENEMIES_BURNER_
#define _TEMPEST_COMPONENTS_ENEMIES_BURNER_

#include "../enemy.h"

class Burner : public Enemy
{
private:
    enum class State
    {
        MovingVertically,
        Jumping,
        Blasting
    };

    // ai
    static constexpr float chargeTime = 1.f;
    static constexpr float attackDuration = 4.f;
    static constexpr float laserRadius = 0.06f;
    static constexpr float jumpInterval = 0.35f;
    static constexpr float jumpOptionalDelay = 0.5f;
    static constexpr float jumpSpeed = 4.f;
    static constexpr float jumpChargeTime = 0.1f;
    static constexpr float horizontalMoveSpeed = 2.5f;
    // looks
    static constexpr Color rayColor = {250, 70, 70, 255};
    static constexpr float rayLineWidth = 15.f;
    // charge particles
    static constexpr Color chargeColor = {255, 50, 255, 255};
    static constexpr float chargeParticleWidthMin = 5.f;
    static constexpr float chargeParticleWidthMax = 6.f;
    static constexpr float chargeParticleRadius = 1.5f;
    static constexpr float chargeParticleTimeMin = 0.5f;    
    static constexpr float chargeParticleTimeMax = 0.7f;
    static constexpr float chargeParticleLength = 0.09f;
    // ray particles
    static constexpr Color particleColor = {250, 50, 50, 255};
    static constexpr float rayParticlesPerUnitLength = 10.f;
    static constexpr float rayParticleWidthMin = 8.f;
    static constexpr float rayParticleWidthMax = 10.f;
    static constexpr float rayParticlesAngle = 15_deg;
    static constexpr float rayParticlesSpeedMin = 0.15f;
    static constexpr float rayParticlesSpeedMax = 0.25f;
    static constexpr float rayParticlesTimeMin = 0.5f;
    static constexpr float rayParticlesTimeMax = 0.6f;
    static constexpr float rayParticleLength = 0.06f;

    State state = State::MovingVertically;
    float progress = 0.f; // charge, blast, jump
    bool charging = true; // blast, jump
    float jumpTimeout = jumpInterval;
    bool jumpChangedSegment = false;
    Pointer<LineTransform> rayTransform = nullptr;
    Pointer<LineRenderer> rayRenderer = nullptr;
    Pointer<ParticleEmiter> rayEmiter = nullptr;
    Vector3 jumpingFrom = {};
    Pointer<MapSegment> jumpingTo = nullptr;
    Pointer<ParticleEmiter> chargeEmiter = nullptr;

    void HandleMovement();
    void HandleJump();
    void HandleBlast();
    void MoveTowardsPlayer();
    void Jump(const Pointer<MapSegment> &target);
    void Blast();
    void UpdateLaser();
    void UpdateChargeParticles();

protected:
    void Awake() override;
    void Update() override;
    void OnDestroy() override;
    void OnPositionChange(const Vector3 &position, const Vector3 &rotation) override;

public:
    Burner();
};

#endif