#include "spiker.h"
#include "tanker.h"
#include "../../models/spiker.h"

void Spiker::Awake()
{
    Enemy::Awake();
    GetTransform()->SetLines(spiker);
    GetRenderer()->SetColor({0, 255, 0, 255});
    GetCollider()->SetWidth(spikeExtension * MapManager::absDepth * 2 + 0.25f);
    spike = ObjectManager::CreateObject()->AddComponent<Spike>();
    spike->SetCurrentSegment(GetCurrentSegment());
}

void Spiker::Update()
{
    float position = GetVerticalPosition();
    if (retreating)
    {
        position += moveSpeed * GameLoop::GetDeltaTime();
        if (position >= 1)
        {
            GetOwner()->Destroy();
            auto tanker = ObjectManager::CreateObject()->AddComponent<Tanker>();
            tanker->SetCurrentSegment(GetCurrentSegment());
        }
    }
    else
    {
        position -= moveSpeed * GameLoop::GetDeltaTime();
        spike->SetLength(1 - position + spikeExtension);
        if (position <= 0)
        {
            retreating = true;
        }
    }
    SetVerticalPosition(position);
}

void Spiker::OnSegmentChange(const Pointer<MapSegment> &previous, const Pointer<MapSegment> &current)
{
    spike->SetCurrentSegment(current);
}

Spiker::Spiker() : Enemy(75) {}