#include "projectile.h"
#include "player.h"
#include "enemy.h"
#include <type_traits>
#include <cassert>

void Projectile::Awake()
{
    Entity::Awake();
    GetCollider()->SetEntityTypes({EntityType::Enemy});
    GetCollider()->RegisterOnCollide([&](const Pointer<Collider> &col)
    {
        bool hit = false;
        auto type = col->GetEntity()->GetEntityType();
        if (hostile)
        {
            if (type == EntityType::FriendlyProjectile)
            {
                if (col->GetEntity().StaticCast<Projectile>()->destroyable)
                {
                    col->GetOwner()->Destroy();
                    hit = true;
                }
            }
            else
            {
                col->GetEntity().StaticCast<Player>()->SetState(PlayerState::Dead);
                hit = true;
            }
        }
        else
        {
            if (type == EntityType::HostileProjectile && col->GetEntity().StaticCast<Projectile>()->destroyable)
            {
                if (col->GetEntity().StaticCast<Projectile>()->destroyable)
                {
                    col->GetOwner()->Destroy();
                    hit = true;
                }
            }
            else
            {
                col->GetEntity().StaticCast<Enemy>()->OnPlayerProjectileHitBase();
                hit = true;
            }
        }
        if (hit)
        {
            targetsHit++;
            if (destroyOnHit)
            {
                GetOwner()->Destroy();
            }
        }
    });
    GetRenderer()->SetAntialias(false);
}

void Projectile::Move()
{
    SetVerticalPosition(GetVerticalPosition() + (direction == VerticalDirection::ToInside ? speed : -speed) * GameLoop::GetDeltaTime());
    if (direction == VerticalDirection::ToInside)
    {
        if (GetVerticalPosition() >= 1)
        {
            GetOwner()->Destroy();
        }
    }
    else
    {
        if (GetVerticalPosition() <= 0)
        {
            GetOwner()->Destroy();
        }
    }
}


Projectile::Projectile() : Entity(EntityType::HostileProjectile) {}

bool Projectile::GetIsHostile() const
{
    return hostile;
}

void Projectile::SetIsHostile(bool hostile)
{
    Projectile::hostile = hostile;
    GetCollider()->SetEntityTypes(hostile ? std::set<EntityType>{EntityType::Player, EntityType::FriendlyProjectile} : std::set<EntityType>{EntityType::Enemy, EntityType::HostileProjectile});
    SetEntityType(hostile ? EntityType::HostileProjectile : EntityType::FriendlyProjectile);
}

float Projectile::GetSpeed() const
{
    return speed;
}

void Projectile::SetSpeed(float speed)
{
    Projectile::speed = speed;
}

bool Projectile::GetDestroyOnHit() const
{
    return destroyOnHit;
}

void Projectile::SetDestroyOnHit(bool destroy)
{
    destroyOnHit = destroy;
}

VerticalDirection Projectile::GetDirection() const
{
    return direction;
}

void Projectile::SetDirection(VerticalDirection direction)
{
    Projectile::direction = direction;
}

bool Projectile::GetDestroyable() const
{
    return destroyable;
}

void Projectile::SetDestroyable(bool destroyable)
{
    Projectile::destroyable = destroyable;
}

unsigned Projectile::GetTargetsHit() const
{
    return targetsHit;
}