#ifndef _TEMPEST_COMPONENTS_CAMERAFOLLOWER_
#define _TEMPEST_COMPONENTS_CAMERAFOLLOWER_

#include <vengine>
#include "mapmanager.h"

class CameraFollower : public Component, public SceneSingleton<CameraFollower>
{
private:
    Vector3 lastValidPos = {};

protected:
    void Awake() override;
    void LateUpdate() override;

public:
    static constexpr float zOffset = 8.f * -MapManager::depthSign;
    Vector3 offset = {};
};

#endif