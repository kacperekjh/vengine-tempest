#ifndef _TEMPEST_COMPONENTS_COLLISIONS_PASSIVECOLLIDER_
#define _TEMPEST_COMPONENTS_COLLISIONS_PASSIVECOLLIDER_

#include "../entity.h"
#include <set>

class Collider : public Component
{
private:
    float previousPosition = -1;
    Pointer<Entity> entity = nullptr;
    std::list<std::function<void(const Pointer<Collider> &)>> callbacks = {};

    void CallCallbacks();

protected:
    std::set<EntityType> types = {};
    float width = 0;
    float offset = 0;

    void Awake() override;
    void LateUpdate() override;
    /// @brief Checks for collisions.
    /// @param segment Segment on which to check for collisions.
    /// @param type Type of entities to check for.
    /// @param from Position to check from.
    /// @param to Position to check to.
    /// @return List of colliding colliders.
    std::vector<Pointer<Collider>> FindCollisions(const Pointer<MapSegment> &segment, EntityType type, float from, float to) const;

public:
    /// @brief Returns what type of entities the collider detects.
    const std::set<EntityType> &GetEntityTypes() const;
    /// @brief Sets the type of entities that the collider can collide with.
    void SetEntityTypes(const std::set<EntityType> &types);
    /// @brief Returns the width (along z-axis) of this collider.
    float GetWidth() const;
    /// @brief Sets the width (along z-axis) of this collider.
    void SetWidth(float width);
    /// @brief Returns the offset (along z-axis) of this collider.
    float GetOffset() const;
    /// @brief Sets the offset (along z-axis) of this collider.
    void SetOffset(float offset);
    /// @brief Returns the entity component that the collider is using.
    const Pointer<Entity> &GetEntity() const;
    /// @brief Checks what the collider collides with on `segment` at `position`.
    std::vector<Pointer<Collider>> FindCollisions(const Pointer<MapSegment> &segment, float position) const;
    /// @brief Checks what the collider collides with on `segment` at `position`.
    std::vector<Pointer<Collider>> FindCollisions(const Pointer<MapSegment> &segment, float position, const std::set<EntityType> &types) const;
    /// @brief Returns the position whre the collider was on the previous frame.
    float GetPreviousPosition() const;
    /// @brief Registers a callback to be called when a collision is detected.
    /// @return Function to unregister the callback.
    std::function<void()> RegisterOnCollide(const std::function<void(const Pointer<Collider> &)> &callback);
};

#endif