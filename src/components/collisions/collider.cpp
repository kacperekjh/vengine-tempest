#include "collider.h"
#include "../entitymanager.h"

void Collider::CallCallbacks()
{
    for (EntityType type : types)
    {
        auto collisions = FindCollisions(GetEntity()->GetCurrentSegment(), type, GetPreviousPosition(), GetEntity()->GetVerticalPosition() * MapManager::absDepth);
        for (auto &collision : collisions)
        {
            for (auto &callback : callbacks)
            {
                callback(collision);
            }
        }
    }
}

void Collider::Awake()
{
    entity = GetOwner()->GetComponent<Entity>();
    if (!entity)
    {
        SDL_LogError(0, "Missing required Entity component for Collider.");
    }
}

void Collider::LateUpdate()
{
    if (!entity)
    {
        return;
    }
    if (callbacks.size() > 0)
    {
        CallCallbacks();
    }
    previousPosition = entity->GetVerticalPosition() * MapManager::absDepth;
}

std::vector<Pointer<Collider>> Collider::FindCollisions(const Pointer<MapSegment> &segment, EntityType type, float from, float to) const
{
    const auto &manager = EntityManager::GetInstance();
    auto &entities = manager->GetEntities(type);
    std::pair<float, float> range1 = {
        from - width / 2 + offset,
        to + width / 2 + offset
    };
    std::vector<Pointer<Collider>> collided;
    for (auto &entity : entities)
    {
        if (!entity)
        {
            continue;
        }
        auto col = entity->GetOwner()->GetComponent<Collider>();
        if (segment != entity->GetCurrentSegment()
            || !col
            || std::find(col->types.begin(), col->types.end(), Collider::entity->GetEntityType()) == col->types.end())
        {
            continue;
        }
        float prev = col->GetPreviousPosition();
        if (prev == -1)
        {
            continue;
        }
        std::pair<float, float> range2 = {
            prev - col->width / 2 + col->offset,
            entity->GetVerticalPosition() * MapManager::absDepth + col->width + col->offset
        };
        if (range1.second < range2.first || range1.first > range2.second)
        {
            continue;
        }
        collided.push_back(col);
    }
    return collided;
}

const std::set<EntityType> &Collider::GetEntityTypes() const
{
    return types;
}

void Collider::SetEntityTypes(const std::set<EntityType> &types)
{
    Collider::types = types;
}

float Collider::GetWidth() const
{
    return width;
}

void Collider::SetWidth(float width)
{
    Collider::width = width;
}

float Collider::GetOffset() const
{
    return offset;
}

void Collider::SetOffset(float offset)
{
    Collider::offset = offset;
}

const Pointer<Entity> &Collider::GetEntity() const
{
    return entity;
}

std::vector<Pointer<Collider>> Collider::FindCollisions(const Pointer<MapSegment> &segment, float position) const
{
    return FindCollisions(segment, position, types);
}

std::vector<Pointer<Collider>> Collider::FindCollisions(const Pointer<MapSegment> &segment, float position, const std::set<EntityType> &types) const
{
    std::vector<Pointer<Collider>> all = {};
    for (EntityType type : types)
    {
        auto collisions = FindCollisions(segment, type, position * MapManager::absDepth, position * MapManager::absDepth);
        all.insert(all.end(), collisions.begin(), collisions.end());
    }
    return all;
}

float Collider::GetPreviousPosition() const
{
    if (!entity)
    {
        return -1;
    }
    return previousPosition == -1 ? entity->GetVerticalPosition() * MapManager::absDepth : previousPosition;
}

std::function<void()> Collider::RegisterOnCollide(const std::function<void(const Pointer<Collider>&)> &callback)
{
    callbacks.push_front(callback);
    auto it = callbacks.begin();
    auto ptr = GetPointer().StaticCast<Collider>();
    return [it, ptr]()
    {
        if (ptr)
        {
            ptr->callbacks.erase(it);
        }
    };
}