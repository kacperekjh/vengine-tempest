#ifndef _TEMPEST_COMPONENTS_PLAYER_
#define _TEMPEST_COMPONENTS_PLAYER_

#include "entity.h"

enum class PlayerState
{
    Alive,
    Transitioning,
    Dying1,
    Dying2,
    Dead
};

class Player : public Entity, public SceneSingleton<Player>
{
private:
    PlayerState state = PlayerState::Alive;
    unsigned projectilesRemaining = 0;
    float projectileRegenTimeout = 0;
    float shootTimeout = 0;
    Line3D model = {};
    Vector3 modelCenter = {};
    bool hasSuperzapper = true;
    size_t superzapperColor = 0;
    float superzapperColorProgress = 0;
    bool usingSuperzapper = false;
    bool superzapperColorChanged = false;
    // transition
    float transitionProgress = 0.f;
    bool transitioningBack = false;
    Vector2 oldPosition = {};
    float oldRotation = {};
    float oldHorizontalPosition = {};
    std::optional<Vector2> newPosition = std::nullopt;
    std::optional<float> newRotation = std::nullopt;

    static constexpr Color superzapperColors[] = {{255, 100, 100, 255}, {255, 255, 100, 255}, {255, 100, 100, 255}, {255, 255, 100, 255}};
    static constexpr size_t superzapperColorCount = sizeof(superzapperColors) / sizeof(Color);
    static constexpr float superZapperColorChangeSpeed = 20.f;
    static constexpr unsigned maxProjectiles = 6;
    static constexpr float projectileRegenTime = 0.25f;
    static constexpr float shootTime = 0.1f; 
    static constexpr size_t centralVertices[] = {1, 5};
    static constexpr size_t rightLeg[] = {2, 3, 4};
    static constexpr size_t leftLeg[] = {0, 6, 7, 8};
    static constexpr float maxCenterMovement = MapManager::length / 3;
    static constexpr float legOffset = MapManager::length / 2; 
    static constexpr float legOffsetDistance = 0.2f;
    static constexpr float centerVerticalOffset = 0.15f;
    static constexpr float movementSpeed = 9.f;
    static constexpr float transitonMoveSpeed = 0.6f;
    static constexpr float transitionDuration = 3.f;
    static constexpr float (*timingFunction)(float) = TimingFunctions::FadeIn;
    static constexpr float explosionRadius = 2.5f;

    void Reset();
    void HandleMovement();
    void HandleAttack();
    void Shoot();
    void HandleAnimation();
    void HandleSuperzapper();
    void HandleTransition();
    void StartTransition();

protected:
    void Awake() override;
    void Update() override;
    void LateUpdate() override;
    void OnSegmentChange(const Pointer<MapSegment> &previous, const Pointer<MapSegment> &current) override;
    void OnPositionChange(const Vector3 &position, const Vector3 &rotation) override;

public:
    static constexpr Color color = {255, 218, 41, 255};

    Player();
    PlayerState GetState() const;
    void SetState(PlayerState state);

    friend class GameManager;
};

#endif