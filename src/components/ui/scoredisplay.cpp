#include "scoredisplay.h"
#include "../gamemanager.h"

void ScoreDisplay::LateUpdate()
{
    if (text)
    {
        text->SetText(std::format("{}", GameManager::GetInstance()->GetScore()));
    }
}