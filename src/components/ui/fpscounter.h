#ifndef _TEMPEST_COMPONENTS_UI_FPSCOUNTER_
#define _TEMPEST_COMPONENTS_UI_FPSCOUNTER_

#include <vengine>

class FPSCounter : public Component
{
protected:
    void Update() override;

public:
    Pointer<UIText> text = nullptr;
    Pointer<UIRoot> root = nullptr;
};

#endif