#ifndef _TEMPEST_COMPONENTS_UI_
#define _TEMPEST_COMPONENTS_UI_

#include <vengine>

class ScoreDisplay : public Component
{
protected:
    void LateUpdate() override;

public:
    Pointer<UIText> text = nullptr;
};

#endif