#include "fpscounter.h"
#include <format>
#include "../../settings.h"

void FPSCounter::Update()
{
    root->SetOpacity(Settings::fpsCounter ? 255 : 0);
    if (text)
    {
        text->SetText(std::format("{:.2f}fps", 1.f / GameLoop::GetDeltaTime()));
    }
}