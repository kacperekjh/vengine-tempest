#ifndef _TEMPEST_COMPONENTS_ENTITYMANAGER_
#define _TEMPEST_COMPONENTS_ENTITYMANAGER_

#include "entity.h"

class EntityManager : public Component, public SceneSingleton<EntityManager>
{
private:
    std::vector<Pointer<Entity>> entities;
    std::map<EntityType, std::vector<Pointer<Entity>>> entitiesSegregated;

protected:
    void Awake() override;

public:
    /// @brief Returns all registered enemies on this scene.
    const std::vector<Pointer<Entity>> &GetEntities();
    /// @brief Returns all registered enemies on thsi scene of specified type. 
    const std::vector<Pointer<Entity>> &GetEntities(EntityType type);

    friend class Entity;
};

#endif