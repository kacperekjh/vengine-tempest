#include "player.h"
#include "../models/player.h"
#include "projectiles/playerprojectile.h"
#include "../renderorder.h"
#include "../particles/explosion.h"
#include "../keys.h"
#include "entitymanager.h"

void Player::Reset()
{
    state = PlayerState::Alive;
    projectilesRemaining = maxProjectiles;
    projectileRegenTimeout = projectileRegenTime;
    shootTimeout = 0;
    GetRenderer()->SetState(true);
    hasSuperzapper = true;
    if (usingSuperzapper)
    {
        MapManager::GetInstance()->ClearOverrideColor();
    }
    usingSuperzapper = false;
}

void Player::HandleMovement()
{
    float vertical = GetVerticalPosition();
    if (vertical < 0 || vertical > 1)
    {
        return;
    }
    int direction = 0;
    if (InputManager::IsHeld(Keys::Get(Keys::Left)))
    {
        direction = -1;
    }
    else if (InputManager::IsHeld(Keys::Get(Keys::Right)))
    {
        direction = 1;
    }
    float position = GetHorizontalPosition() + movementSpeed * direction * GameLoop::GetDeltaTime();
    while (position > 1 && GetCurrentSegment()->GetNext())
    {
        SetCurrentSegment(GetCurrentSegment()->GetNext());
        position--;
    }
    while (position < 0 && GetCurrentSegment()->GetPrevious())
    {
        SetCurrentSegment(GetCurrentSegment()->GetPrevious());
        position++;
    }
    position = std::clamp(position, 0.f, 1.f);
    SetHorizontalPosition(position);
}

void Player::HandleAttack()
{
    // shooting
    if (projectilesRemaining < maxProjectiles)
    {
        projectileRegenTimeout -= GameLoop::GetDeltaTime();
    }
    if (projectileRegenTimeout <= 0)
    {
        projectileRegenTimeout = projectileRegenTime;
        projectilesRemaining++;
    }
    shootTimeout -= GameLoop::GetDeltaTime();
    if (shootTimeout <= 0 && projectilesRemaining > 0 && InputManager::IsHeld(Keys::Get(Keys::Shoot)))
    {
        Shoot();
        shootTimeout = shootTime;
        projectilesRemaining--;
    }
}

void Player::Shoot()
{
    const auto &object = ObjectManager::CreateObject();
    auto projectile = object->AddComponent<PlayerProjectile>();
    projectile->SetCurrentSegment(GetCurrentSegment());
    projectile->SetVerticalPosition(GetVerticalPosition());
}

void Player::HandleAnimation()
{
    const float position = GetHorizontalPosition();
    float t = std::clamp((position - 0.5f + legOffsetDistance) / -(0.5f - legOffsetDistance), 0.f, 1.f);
    float leftLegOffset = std::lerp(0.f, -Player::legOffset, t);
    for (size_t vertex : leftLeg)
    {
        model[vertex] = ::player[0][vertex] + Vector3(0, leftLegOffset, 0);
    }
    t = std::clamp((position - 0.5f - legOffsetDistance) / (1 - 0.5f - legOffsetDistance), 0.f, 1.f);
    float rightLegOffset = std::lerp(0.f, Player::legOffset, t);
    for (size_t vertex : rightLeg)
    {
        model[vertex] = ::player[0][vertex] + Vector3(0, rightLegOffset, 0);
    }
    Vector3 modelCenter = {-std::max(leftLegOffset, rightLegOffset) * centerVerticalOffset, (position - 0.5f) * 2 * maxCenterMovement, 0};
    for (size_t vertex : centralVertices)
    {
        model[vertex] = ::player[0][vertex] + modelCenter;
    }
    modelCenter = modelCenter.Rotate({0, 0, GetCurrentSegment()->GetAngle()});
    GetTransform()->SetLines(model);
}

void Player::HandleSuperzapper()
{
    if (usingSuperzapper)
    {
        superzapperColorProgress += superZapperColorChangeSpeed * GameLoop::GetDeltaTime();
        if (superzapperColorProgress >= 1)
        {
            superzapperColor++;
            superzapperColorProgress = 0;
            superzapperColorChanged = true;
        }
        if (superzapperColor >= superzapperColorCount)
        {
            usingSuperzapper = false;
            MapManager::GetInstance()->ClearOverrideColor();
        }
        else
        {
            MapManager::GetInstance()->OverrideColor(superzapperColors[superzapperColor]);
            superzapperColorChanged = false;
        }
    }
    if (hasSuperzapper && InputManager::IsHeld(Keys::Get(Keys::Superzapper)))
    {
        usingSuperzapper = true;
        hasSuperzapper = false;
        superzapperColor = 0;
        superzapperColorProgress = 0;
        superzapperColorChanged = true;
        auto enemies = EntityManager::GetInstance()->GetEntities(EntityType::Enemy);
        for (const auto &enemy : enemies)
        {
            if (enemy)
            {
                enemy->GetOwner()->Destroy();
            }
        }
    }
}

void Player::HandleTransition()
{
    if (state != PlayerState::Transitioning)
    {
        return;
    }
    if (!transitioningBack)
    {
        constexpr float endPos = transitonMoveSpeed * transitionDuration;
        transitionProgress += GameLoop::GetDeltaTime() / transitionDuration;
        transitionProgress = std::min(transitionProgress, 1.f);
        SetVerticalPosition(std::lerp(0.f, endPos, timingFunction(transitionProgress)));
        if (transitionProgress >= 1)
        {
            transitioningBack = true;
            newPosition = std::nullopt;
            newRotation = std::nullopt;
            oldHorizontalPosition = GetHorizontalPosition();
        }
    }
    else
    {
        constexpr float startPos = -transitonMoveSpeed * transitionDuration;
        transitionProgress -= GameLoop::GetDeltaTime() / transitionDuration;
        transitionProgress = std::max(transitionProgress, 0.f);
        float t = timingFunction(transitionProgress);
        SetVerticalPosition(std::lerp(0.f, startPos, t));
        if (transitionProgress <= 0)
        {
            state = PlayerState::Alive;
            return;
        }
        if (newPosition && newRotation)
        {
            Vector2 pos = Vector2::Lerp(newPosition.value(), oldPosition, transitionProgress);
            GetOwner()->position.x = pos.x;
            GetOwner()->position.y = pos.y;
            GetOwner()->rotation.z = AngleLerp(newRotation.value(), oldRotation, t);
            SetHorizontalPosition(std::lerp(0.5f, oldHorizontalPosition, t));
        }
    }
}

void Player::StartTransition()
{
    state = PlayerState::Transitioning;
    transitioningBack = false;
    transitionProgress = 0.f;
}

void Player::Awake()
{
    Entity::Awake();
    SetInstance();
    model = ::player[0];
    GetRenderer()->SetColor(color);
    GetCollider()->SetWidth(0.5f);
    GetCollider()->SetEntityTypes({EntityType::HostileProjectile, EntityType::Enemy});
    allowUnrestrictedVerticalMovement = true;
}

void Player::Update()
{
    HandleTransition();
    if (!GetCurrentSegment() || state >= PlayerState::Dying1)
    {
        return;
    }
    HandleMovement();
    HandleAttack();
    HandleSuperzapper();
}

void Player::LateUpdate()
{
    if (GetVerticalPosition() > 0)
    {
        GetRenderer()->SetPriority(RenderOrder::PlayerFar);
    }
    else
    {
        GetRenderer()->SetPriority(RenderOrder::Player);
    }
    HandleAnimation();
}

void Player::OnSegmentChange(const Pointer<MapSegment> &previous, const Pointer<MapSegment> &current)
{
    MapManager::GetInstance(GetScene())->SetSelectedSegment(current);
    if (state == PlayerState::Transitioning && !newPosition)
    {
        oldPosition = (Vector2)GetOwner()->position;
        oldRotation = GetOwner()->rotation.z;
        newPosition = (Vector2)CalculatePosition(current, 0.f, 0.5f);
        newRotation = current->GetAngle();
    }
}

void Player::OnPositionChange(const Vector3 &position, const Vector3 &rotation)
{
    if (GetVerticalPosition() >= 0)
    {
        GetOwner()->position = CalculatePosition(GetCurrentSegment(), GetVerticalPosition(), 0.5f);
        GetOwner()->rotation = rotation;
    }
    else
    {
        GetOwner()->position.z = CalculatePosition(GetCurrentSegment(), GetVerticalPosition(), 0.5f).z;
    }
}

Player::Player() : Entity(EntityType::Player) {}

void Player::SetState(PlayerState state)
{
    if (state == PlayerState::Dead && Player::state != PlayerState::Dead)
    {
        Explosion explosion = Explosion();
        explosion.radius = explosionRadius;
        explosion.particleCount = 35;
        explosion.Explode(GetScene(), GetOwner()->GetGlobalPosition() + modelCenter);
        GetRenderer()->SetState(false);
    }
    Player::state = state;
}

PlayerState Player::GetState() const
{
    return state;
}