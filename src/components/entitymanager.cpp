#include "entitymanager.h"
#include <algorithm>

void EntityManager::Awake()
{
    SetInstance();
}

const std::vector<Pointer<Entity>> &EntityManager::GetEntities()
{
    return entities;
}

const std::vector<Pointer<Entity>> &EntityManager::GetEntities(EntityType type)
{
    return entitiesSegregated[type];
}
