#ifndef _TEMPEST_COMPONENTS_GAMEMANAGER_
#define _TEMPEST_COMPONENTS_GAMEMANAGER_

#include <vengine>
#include "player.h"

enum class GameState
{
    Stopped,
    Starting,
    Running,
    PreTransition,
    Transitioning
};

class GameManager : public Component, public SceneSingleton<GameManager>
{
private:
    std::list<std::function<void(GameState)>> callbacks = {};
    std::vector<Line2D> maps = {};
    unsigned currentStage = 0;
    GameState state = GameState::Stopped;
    Pointer<Player> player = nullptr;
    unsigned remainingEnemies;
    float spawnTimeout;
    float progress = 0.f; // flying progress, pretransition timeout
    static constexpr float preTransitionTime = 1.5f;
    bool changedStage = false;
    float enemySpawnInterval = 0.f;
    unsigned long score = 0; 

    void StartStage(unsigned stage);
    void ResetPlayer();
    void LoseRound();
    void SpawnEnemies();
    void HandleRunning();
    void HandleTransition();
    void HandleStart();
    void EnterPreTransition();
    void SetState(GameState state);

protected:
    void Awake() override;
    void Update() override;
    void LateUpdate() override;

public:
    static unsigned GetEnemyCount(unsigned stage);
    static float GetEnemySpawnInterval(unsigned stage);

    static constexpr float spawnIntervalDeviation = 0.15f;

    GameState GetGameState() const;
    void StartGame(const std::vector<Line2D> &tunnels);
    std::function<void(void)> RegisterStateChangeCallback(const std::function<void(GameState)> &callback);
    void IncrementScore(unsigned long score);
    unsigned long GetScore() const;
    unsigned GetCurrentStage() const;
};

#endif