#ifndef _TEMPEST_COMPONENTS_MAPMANAGER_
#define _TEMPEST_COMPONENTS_MAPMANAGER_

#include "../mapsegment.h"
#include <optional>

class MapManager : public Component, public SceneSingleton<MapManager>
{
private:
    struct Lines
    {
        Pointer<LineRenderer> line1, line2;
    };

    std::vector<Pointer<MapSegment>> segments;
    std::vector<Lines> lines = {};
    Pointer<GameObject> innerRing = nullptr;
    Pointer<GameObject> outerRing = nullptr;
    std::optional<size_t> selected = std::nullopt;
    bool overrodeColor = false;
    bool closed = false;

    void Select(const Pointer<LineRenderer> &renderer);
    void Unselect(const Pointer<LineRenderer> &renderer);
    Pointer<LineRenderer> CreateLine(Vector2 point);
    const Pointer<GameObject> &CreateRing(const Line2D &line, float depth, int priority);

protected:
    void Awake() override;

public:
    static constexpr Color defaultColor = {25, 25, 255, 255};
    static constexpr Color selectedColor = {255, 218, 41, 255};
    static constexpr float depth = 60.f;
    static constexpr float absDepth = std::abs(depth);
    static constexpr float depthSign = depth / absDepth;
    static constexpr float length = 1.5f;

    ~MapManager();
    /// @brief Creates a continous line of segments from `line`. If the first and the last points are the same, the segments will form a closed loop.
    /// @param scene Scene that the segment objects will be created on.
    /// @param points Points of the line given in clockwise order.
    const std::vector<Pointer<MapSegment>> &CreateMap(const Line2D &line);
    /// @brief Returns all existing map segments.
    const std::vector<Pointer<MapSegment>> &GetSegments() const;
    /// @brief Destroys all map segments.
    void DestroyMap();
    /// @brief Returns the currently selected map segment.
    const Pointer<MapSegment> &GetSelectedSegment() const;
    /// @brief Change the currently selected map segment.
    void SetSelectedSegment(const Pointer<MapSegment> &segment);
    /// @brief Changes color of all map segments to `color`.
    void OverrideColor(Color color);
    /// @brief Returns all map segments to their default color.
    void ClearOverrideColor();
    /// @brief Returns true if the map forms a closed loop.
    bool IsMapClosed() const;
};

#endif