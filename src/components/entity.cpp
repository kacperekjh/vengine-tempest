#include "entity.h"
#include "entitymanager.h"
#include <algorithm>
#ifndef NDEBUG
#include "player.h"
#include "enemy.h"
#include "projectile.h"
#endif

void Entity::Register()
{
    // Enforce that the entity type can be casted to a valid component of its type.
    #ifndef NDEBUG
    switch (type)
    {
    case EntityType::Player:
        assert(GetPointer().DynamicCast<Player>().GetState());
        break;
    case EntityType::Enemy:
        assert(GetPointer().DynamicCast<Enemy>().GetState());
        break;
    case EntityType::FriendlyProjectile:
    case EntityType::HostileProjectile:
        assert(GetPointer().DynamicCast<Projectile>().GetState());
        break;
    }
    #endif
    auto manager = EntityManager::GetInstance(GetScene());
    auto ptr = GetPointer().StaticCast<Entity>();
    manager->entities.push_back(ptr);
    manager->entitiesSegregated[type].push_back(ptr);
}

void Entity::Unregister()
{
    auto manager = EntityManager::GetInstance(GetScene());
    auto ptr = GetPointer().StaticCast<Entity>();
    auto &entities1 = manager->entities;
    entities1.erase(std::find(entities1.begin(), entities1.end(), ptr));
    auto &entities2 = manager->entitiesSegregated[type];
    entities2.erase(std::find(entities2.begin(), entities2.end(), ptr));
}

void Entity::OnSegmentChange(const Pointer<MapSegment> &previous, const Pointer<MapSegment> &current) {}

void Entity::OnPositionChange(const Vector3 &position, const Vector3 &rotation)
{
    GetOwner()->position = position;
    GetOwner()->rotation = rotation;
}

void Entity::UpdatePosition()
{
    if (segment)
    {
        Vector3 rotation = {0, 0, segment->GetAngle()};
        OnPositionChange(CalculatePosition(GetCurrentSegment(), verticalPosition, horizontalPosition), rotation);
    }
}

Entity::Entity(EntityType type) : type(type) {}

void Entity::Awake()
{
    Register();
    collider = AddComponent<Collider>();
    transform = AddComponent<LineTransform>();
    renderer = AddComponent<LineRenderer>();
}

void Entity::OnDestroy()
{
    Unregister();
}

void Entity::SetCurrentSegment(const Pointer<MapSegment> &segment)
{
    if (Entity::segment != segment)
    {
        auto previous = Entity::segment;
        Entity::segment = segment;
        OnSegmentChange(previous, segment);
        UpdatePosition();
    }
}

const Pointer<MapSegment> &Entity::GetCurrentSegment() const
{
    return segment;
}

float Entity::GetVerticalPosition() const
{
    return verticalPosition;
}

void Entity::SetVerticalPosition(float position)
{
    if (allowUnrestrictedVerticalMovement)
    {
        Entity::verticalPosition = position;
    }
    else
    {
        Entity::verticalPosition = std::clamp(position, 0.f, 1.f);
    }
    UpdatePosition();
}

float Entity::GetHorizontalPosition() const
{
    return horizontalPosition;
}

void Entity::SetHorizontalPosition(float position)
{
    horizontalPosition = position;
    UpdatePosition();
}

EntityType Entity::GetEntityType() const
{
    return type;
}

void Entity::SetEntityType(EntityType type)
{
    Unregister();
    Entity::type = type;
    Register();
}

const Pointer<Collider> Entity::GetCollider() const
{
    return collider;
}

const Pointer<LineTransform> Entity::GetTransform() const
{
    return transform;
}

const Pointer<LineRenderer> Entity::GetRenderer() const
{
    return renderer;
}

Vector3 Entity::CalculatePosition(const Pointer<MapSegment> &segment, float verticalPos, float horizontalPos)
{
    const auto &points = segment->GetPoints();
    Vector3 position = Vector2::Lerp(points.first, points.second, horizontalPos);
    position.z = MapManager::depth * verticalPos;
    return position;
}