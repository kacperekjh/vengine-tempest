#include "enemy.h"
#include "../renderorder.h"
#include "projectiles/enemyprojectile.h"
#include "gamemanager.h"
#include "../particles/explosion.h"

void Enemy::OnPlayerProjectileHitBase()
{
    GameManager::GetInstance(GetScene())->IncrementScore(score);
    OnPlayerProjectileHit();
}

void Enemy::Awake()
{
    Entity::Awake();
    GetCollider()->SetEntityTypes(defaultColliderTypes);
    SetVerticalPosition(1);
}

void Enemy::LateUpdate()
{
    if (GetVerticalPosition() > 0)
    {
        GetRenderer()->SetPriority(RenderOrder::EnemyFar);
    }
    else
    {
        GetRenderer()->SetPriority(RenderOrder::Enemy);
    }
    OnRenderPriorityChange(GetRenderer()->GetPriority());
}

void Enemy::Shoot()
{
    auto projectile = ObjectManager::CreateObject()->AddComponent<EnemyProjectile>();
    projectile->SetVerticalPosition(GetVerticalPosition());
    projectile->SetCurrentSegment(GetCurrentSegment());
}

void Enemy::MakeImmune(bool immune)
{
    GetCollider()->SetEntityTypes(immune ? immuneColliderTypes : defaultColliderTypes);
}

void Enemy::Explode(float radius)
{
    Explosion explosion = Explosion();
    explosion.radius = radius;
    explosion.timeMin = 0.3f;
    explosion.timeMax = 0.6f;
    explosion.colors = Gradient<Color>({
        {0.f, {255, 255, 255, 255}},
        {0.75f, {255, 255, 255, 80}},
        {1.f, {150, 150, 150, 0}}
    });
    explosion.lineWidthMin = 5.f;
    explosion.lineWidthMax = 7.f;
    explosion.particleCount = 12;
    explosion.Explode(GetScene(), GetOwner()->GetGlobalPosition());
}

void Enemy::OnRenderPriorityChange(int priority) {}

void Enemy::OnPlayerProjectileHit()
{
    GetOwner()->Destroy();
    Explode();
}

Enemy::Enemy(unsigned long score) : Entity(EntityType::Enemy), score(score) {}

bool Enemy::IsDefeated() const
{
    return defeated;
}

bool Enemy::GetDestroyOnEnd() const
{
    return destroyOnGameEnd;
}