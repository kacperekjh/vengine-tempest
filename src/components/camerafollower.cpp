#include "camerafollower.h"
#include "player.h"

void CameraFollower::Awake()
{
    SetInstance();
    auto camera = AddComponent<Camera>();
    camera->SetProjection(Projection::Perspective);
    camera->SetFarClipPlane(150.f);
}

void CameraFollower::LateUpdate()
{
    auto player = Player::GetInstance();
    if (player->GetState() > PlayerState::Transitioning)
    {
        GetOwner()->position = lastValidPos;
    }
    else
    {
        lastValidPos = GetOwner()->position = Vector3(0, 0, player->GetOwner()->position.z + zOffset) + offset;
    }
}
