#include "menumanager.h"
#include "../particles/spaceparticles.h"
#include "../transition.h"
#include "keys.h"
#include "settings.h"

MenuManager::Button MenuManager::CreateButton(const Pointer<UIElement> &parent, std::string_view text, const std::function<void()> &callback, const UISizeExpression &width) const
{
    constexpr Color frontSelected = {40, 40, 255, 255};
    constexpr Color frontUnselected = {255, 255, 255, 255};
    constexpr Color backSelected = {220, 10, 10, 255};
    constexpr Color backUnselected = {150, 40, 40, 255};
    constexpr unsigned lineWidth = 2;
    const UISize size = {width, 60_px};
    const UISize offset = {6_px, 7_px}; 
    const UISize childSize = {100_pct - offset.x, 100_pct - offset.y};

    auto button = UIManager::CreateElement<UIButton>(parent);
    button->SetCallback(callback);
    button->SetSize({size.x + offset.x, size.y + offset.y});
    button->SetMargin({0_px, 5_px, 0_px, 5_px});
    // back
    auto back = UIManager::CreateElement<UIRectangle>(button);
    back->SetColor(backUnselected);
    back->SetLineWidth(lineWidth);
    back->SetSize(childSize);
    back->SetMargin({offset.x, offset.y, 0_px, 0_px});
    //front
    auto front = UIManager::CreateElement<UIRectangle>(button);
    front->SetColor(frontUnselected);
    front->SetLineWidth(lineWidth);
    front->SetSize(childSize);

    //text
    auto text2 = UIManager::CreateElement<UIText>(button);
    text2->SetFontSize(30);
    text2->SetText(text);
    text2->SetTextAlignment(UIAlignment::Center, UIAlignment::Center);
    text2->SetSize(childSize);

    // on select
    button->SetOnFocus([front, back]()
    {
        front->SetColor(frontSelected);
        front->SetLineWidth(lineWidth + 1);
        back->SetColor(backSelected);
    });

    // on unselect
    button->SetOnUnfocus([front, back]()
    {
        front->SetColor(frontUnselected);
        front->SetLineWidth(lineWidth);
        back->SetColor(backUnselected);
    });
    return {button, text2};
}

void MenuManager::CreateWorld()
{
    camera = ObjectManager::CreateObject(GetScene());
    camera->position = {0, 0, -cameraDistance};
    camera->AddComponent<Camera>()->SetProjection(Projection::Perspective);

    static constexpr float spawnDepth = 25.f;
    static constexpr float edgeExtension = 1.5f;
    auto emiter = ObjectManager::CreateObject(GetScene())->AddComponent<ParticleEmiter>();
    // spawn
    auto updateSpawnSize = [camera = this->camera, emiter](Vector2Int){
        if (!camera || !emiter)
        {
            return;
        }
        Vector2 size = camera->GetComponent<Camera>()->GetViewportSize(cameraDistance + spawnDepth) + Vector2(edgeExtension, edgeExtension) * 2;
        size /= 2;
        emiter->spawn.getPosition = SpawnBehaviors::RandomBox({-size.x, -size.y, 0.f}, {size.x, size.y, spawnDepth});
    };
    Renderer::RegisterOnResolutionChange(updateSpawnSize);
    updateSpawnSize({});
    emiter->particlesPerSecond = 11.f;
    SpaceParticles particles = SpaceParticles();
    emiter->visual = particles.GetVisualData();
    emiter->behavior = particles.GetBehaviorData();

    emiter->EmitBurst(60);
}

void MenuManager::CreateMainMenu(const Pointer<UIRoot> &root)
{
    auto mainContainer = UIManager::CreateElement<UIForm>(root);
    mainContainer->SetSize({100_pct, 100_pct});
    mainContainer->SetDirection(UIDirection::TopToBottom);
    mainContainer->SetAlignment(UIAlignment::Center, UIAlignment::Center);

    // logo
    auto logoFont = ResourceManager::OpenResourceFromFile<Font>("fonts/cilica.regular.ttf");
    static constexpr std::string_view logoText = "Tempestous";
    static constexpr int logoSize = 140;
    auto container = UIManager::CreateElement<UIStackingContainer>(mainContainer);
    container->SetAlignment(UIAlignment::Center, UIAlignment::Center);
    container->SetMargin({0_px, 0_px, 0_px, 70_px});
    // back
    auto text = UIManager::CreateElement<UIText>(container);
    text->SetFont(logoFont);
    text->SetFontSize(logoSize);
    text->SetText(logoText);
    text->SetTextColor({50, 50, 255, 255});
    text->SetMargin({13_px, 18_px, 0_px, 0_px});
    // front
    text = UIManager::CreateElement<UIText>(container);
    text->SetFont(logoFont);
    text->SetFontSize(logoSize);
    text->SetText(logoText);

    auto manager = GetPointer().StaticCast<MenuManager>();
    CreateButton(mainContainer, "Graj", [manager]()
    {
        if (manager)
        {
            manager->GoToGame();
        }
    });
    CreateButton(mainContainer, "Ustawienia", [manager]()
    {
        if (manager)
        {
            manager->GoToPage(Page::Settings);
        }
    });
    CreateButton(mainContainer, "Wyjdź", []()
    {
        GameLoop::EndLoop();
    });
}

void MenuManager::GoToPage(Page page)
{
    switch (page)
    {
    case Page::Main:
        fadingTo = mainMenu;
        break;
    case Page::Settings:
        fadingTo = settings;
        break;
    default:
        throw std::invalid_argument("Invalid page.");
    }
    progress = 1.f;
    fade = FadingStage::FadingOut;
}

void MenuManager::GoToGame()
{
    progress = 1.f;
    fade = FadingStage::FadingOut;
    stage = EnteringGameStage::FadingOut;
}

void MenuManager::CreateSettings(const Pointer<UIRoot> &root)
{
    auto mainContainer = UIManager::CreateElement<UIForm>(root);
    mainContainer->SetSize({100_pct, 100_pct});
    mainContainer->SetDirection(UIDirection::TopToBottom);
    mainContainer->SetPadding({10_pct, 10_px, 10_pct, 10_px});

    const UISizeExpression returnButtonWidth = 10_pct;
    auto topContainer = UIManager::CreateElement<UIContainer>(mainContainer);
    topContainer->SetSize({100_pct, size_auto});
    topContainer->SetDirection(UIDirection::LeftToRight);
    topContainer->SetPadding({0_px, 0_px, returnButtonWidth, 0_px});
    topContainer->SetMargin({0_px, 0_px, 0_px, 50_px});

    // return
    auto manager = GetPointer().StaticCast<MenuManager>();
    CreateButton(topContainer, "Wróć", [manager]()
    {
        if (manager)
        {
            manager->GoToPage(Page::Main);
        }
    }, returnButtonWidth);

    // text
    auto text = UIManager::CreateElement<UIText>(topContainer);
    text->SetText("Ustawienia");
    text->SetFontSize(40);
    text->SetTextAlignment(UIAlignment::Center, UIAlignment::Begin);
    text->SetSize({100_pct, size_auto});

    // fullscreen
    auto button = CreateButton(mainContainer, "Pełen ekran (wyłączony)", {}, 400_px);
    button.button->SetCallback([text = button.text]()
    {
        bool fullscreen = Settings::GetFullScreen();
        text->SetText(fullscreen ? "Pełen ekran (wyłączony)" : "Pełen ekran (włączony)");
        Settings::SetFullScreen(!fullscreen);
    });

    // fps counter
    button = CreateButton(mainContainer, "Licznik FPS (wyłączony)", {}, 400_px);
    button.button->SetCallback([text = button.text]()
    {
        text->SetText(Settings::fpsCounter ? "Licznik FPS (wyłączony)" : "Licznik FPS (włączony)");
        Settings::fpsCounter = !Settings::fpsCounter;
    });
}

void MenuManager::HandleFading()
{
    if (fade == FadingStage::NoFading)
    {
        return;
    }
    const auto &scene = GetScene();
    if (fade == FadingStage::FadingOut)
    {
        progress -= GameLoop::GetDeltaTime() * fadeSpeed;
        if (progress <= 0)
        {
            progress = 0.f;
            if (stage == EnteringGameStage::FadingOut)
            {
                stage = EnteringGameStage::Flying;
                fade = FadingStage::NoFading;
                progress = 0.f;
            }
            else
            {
                fadingTo->SetOpacity(0);
                scene->SetUIRoot(fadingTo);
                fade = FadingStage::FadingIn;
            }
            return;
        }
    }
    else
    {
        progress += GameLoop::GetDeltaTime() * fadeSpeed;
        if (progress >= 1)
        {
            scene->GetUIRoot()->SetOpacity(255);
            fade = FadingStage::NoFading;
            return;
        }
    }
    float t = std::clamp(Transition::timingFunction(progress), 0.f, 1.f);
    scene->GetUIRoot()->SetOpacity(uint8_t(255 * t));
}

void MenuManager::HandleGameEnter()
{
    if (stage != EnteringGameStage::Flying)
    {
        return;
    }
    constexpr float endOffset = Transition::flySpeed * Transition::flyDuration;
    float delta = GameLoop::GetDeltaTime();
    progress += delta / Transition::flyDuration;
    float t = TimingFunctions::FadeIn(progress);
    camera->position = Vector3(0, 0, std::lerp(-cameraDistance, -cameraDistance + endOffset, t));
    if (progress >= 1)
    {
        ObjectManager::GoToScene("main");
    }
}

void MenuManager::Awake()
{
    SetInstance();
    CreateWorld();
    mainMenu = GetScene()->GetUIRoot();
    settings = UIManager::CreateElement<UIRoot>(nullptr);
    CreateMainMenu(mainMenu);
    CreateSettings(settings);
}

void MenuManager::Update()
{
    HandleFading();
    HandleGameEnter();
}

void MenuManager::OnSceneLoaded()
{
    fade = FadingStage::NoFading;
    stage = EnteringGameStage::NotEntering;
    GetScene()->SetUIRoot(mainMenu);
    openPage = Page::Main;
    mainMenu->SetOpacity(255);
    camera->position = {0, 0, -cameraDistance};
}

MenuManager::~MenuManager()
{
    mainMenu.Delete();
    settings.Delete();
}