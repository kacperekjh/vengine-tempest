#include "gamemanager.h"
#include "entitymanager.h"
#include "mapmanager.h"
#include "../transition.h"
#include "camerafollower.h"
#include "enemy.h"
#include "../enemyspawner.h"

void GameManager::StartStage(unsigned stage)
{
    currentStage = stage;
    auto map = MapManager::GetInstance(GetScene());
    map->CreateMap(maps[(stage - 1) % maps.size()]);
    player->SetCurrentSegment(map->GetSegments()[0]);
    enemySpawnInterval = spawnTimeout = GetEnemySpawnInterval(stage);
    remainingEnemies = GetEnemyCount(stage);
    auto entities = EntityManager::GetInstance(GetScene())->GetEntities();
    for (auto &entity : entities)
    {
        if (entity && entity->GetEntityType() != EntityType::Player)
        {
            entity->GetOwner()->Destroy();
        }
    }
}

void GameManager::ResetPlayer()
{
    if (!player)
    {
        auto playerObject = ObjectManager::CreateObject(GetScene());
        player = playerObject->AddComponent<Player>();
    }
    player->Reset();
}

void GameManager::LoseRound()
{
    SetState(GameState::Stopped);
    SDL_Log("Player died. Game over.");
}

void GameManager::SpawnEnemies()
{
    spawnTimeout -= GameLoop::GetDeltaTime();
    if (spawnTimeout > 0 || remainingEnemies == 0)
    {
        return;
    }
    EnemySpawner::SpawnRandomEnemy();
    spawnTimeout = enemySpawnInterval + Random::GetRandomNumber(-spawnIntervalDeviation, spawnIntervalDeviation);
    remainingEnemies--;
}

void GameManager::HandleRunning()
{
    if (player->GetState() != PlayerState::Alive)
    {
        return;
    }
    SpawnEnemies();
    if (remainingEnemies == 0)
    {
        size_t enemyCount = 0;
        for (const auto &enemy : EntityManager::GetInstance()->GetEntities(EntityType::Enemy))
        {
            if (!enemy)
            {
                continue;
            }
            if (!enemy.StaticCast<Enemy>()->IsDefeated())
            {
                enemyCount++;
            }
        }
        if (enemyCount == 0)
        {
            EnterPreTransition();
        }
    }
}

void GameManager::HandleTransition()
{
    if (state == GameState::PreTransition)
    {
        progress -= GameLoop::GetDeltaTime();
        if (progress <= 0)
        {
            if (player->GetState() == PlayerState::Alive)
            {
                SetState(GameState::Transitioning);
                progress = 0.f;
                player->StartTransition();
                changedStage = false;
            }
            else
            {
                LoseRound();
            }
        }
    }
    else
    {
        bool transitioning = player->GetState() == PlayerState::Transitioning;
        if (!transitioning)
        {
            SetState(GameState::Running);
            ResetPlayer();
        }
        else if (!changedStage && player->transitioningBack)
        {
            StartStage(currentStage + 1);
            changedStage = true;
        }
    }
}

void GameManager::HandleStart()
{
    progress += GameLoop::GetDeltaTime();
    progress = std::min(progress, 1.f);
    constexpr Vector3 offset = {0, 0, Transition::flySpeed * Transition::flyDuration * -MapManager::depthSign};
    auto follower = CameraFollower::GetInstance();
    follower->offset = Vector3::Lerp(Vector3(), offset, Transition::timingFunction(1 - progress));
    if (progress >= 1)
    {
        SetState(GameState::Running);
    }
}

void GameManager::EnterPreTransition()
{
    SetState(GameState::PreTransition);
    progress = preTransitionTime;
}

void GameManager::SetState(GameState state)
{
    GameManager::state = state;
    for (const auto &callback : callbacks)
    {
        if (callback)
        {
            callback(state);
        }
    }
}

void GameManager::Awake()
{
    SetInstance();
}

void GameManager::Update()
{
    if (state == GameState::Starting)
    {
        HandleStart();
    }
}

void GameManager::LateUpdate()
{
    if (state == GameState::Stopped || state == GameState::Starting)
    {
        return;
    }
    if (player->state == PlayerState::Dead)
    {
        LoseRound();
        return;
    }
    switch (state)
    {
    case GameState::Running:
        HandleRunning();
        break;
    case GameState::PreTransition:
    case GameState::Transitioning:
        HandleTransition();
        break;
    default:
        throw std::runtime_error("Invalid GameState.");
    }
}

unsigned GameManager::GetEnemyCount(unsigned stage)
{
    float n = stage;
    n = 5.f + n / 4.f + std::log(n) * 4.f;
    return (unsigned)std::ceil(n);
}

float GameManager::GetEnemySpawnInterval(unsigned stage)
{
    float n = stage;
    n = std::max<float>(1.5f - std::log(n) / 4.f, 0.4f);
    return n;
}

GameState GameManager::GetGameState() const
{
    return state;
}

void GameManager::StartGame(const std::vector<Line2D> &maps)
{
    assert(maps.size() > 0);
    if (state != GameState::Stopped)
    {
        SDL_LogWarn(0, "Game has already been started.");
        return;
    }
    GameManager::maps = maps;
    ResetPlayer();
    StartStage(1);
    SetState(GameState::Starting);
    progress = 0.f;
    score = 0;
}

std::function<void(void)> GameManager::RegisterStateChangeCallback(const std::function<void(GameState)> &callback)
{
    callbacks.push_front(callback);
    auto it = callbacks.begin();
    auto ptr = GetPointer().StaticCast<GameManager>();
    return [it, manager = ptr]()
    {
        if (manager)
        {
            manager->callbacks.erase(it);
        }
    };
}

void GameManager::IncrementScore(unsigned long score)
{
    GameManager::score += score;
}

unsigned long GameManager::GetScore() const
{
    return score;
}

unsigned GameManager::GetCurrentStage() const
{
    return currentStage;
}