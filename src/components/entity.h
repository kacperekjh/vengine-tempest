#ifndef _TEMPEST_COMPONENTS_ENTITY_
#define _TEMPEST_COMPONENTS_ENTITY_

#include <vengine>
#include "mapmanager.h"

class Collider;

enum class EntityType
{
    Player,
    Enemy,
    FriendlyProjectile,
    HostileProjectile
};

/// @brief Base class for all entities bound to map segments.
class Entity : public Component
{
private:
    Pointer<MapSegment> segment;
    Pointer<Collider> collider = nullptr;
    Pointer<LineTransform> transform = nullptr;
    Pointer<LineRenderer> renderer = nullptr;
    float verticalPosition = 0;
    float horizontalPosition = 0.5f;
    float width = 0;
    EntityType type;

    void Register();
    void Unregister();

protected:
    /// @brief If true, the entity is not limited to the vertical position range of [0, 1].
    bool allowUnrestrictedVerticalMovement = false;

    Entity(EntityType type);

    void Awake() override;
    void OnDestroy() override;

    /// @brief Function that runs when the segment that the entity is on changes.
    virtual void OnSegmentChange(const Pointer<MapSegment> &previous, const Pointer<MapSegment> &current);
    /// @brief Funciton that runs when the position or roration of the entity changes.
    virtual void OnPositionChange(const Vector3 &position, const Vector3 &rotation);
    /// @brief Forces the entity to update its position and rotation.
    void UpdatePosition();

public:
    /// @brief Returns the segment that the enemy is currently on.
    const Pointer<MapSegment> &GetCurrentSegment() const;
    /// @brief Changes the segment that the enemy is currently on.
    void SetCurrentSegment(const Pointer<MapSegment> &segment);
    /// @brief Returns the position of this object on a segment ie. how deep it is. This position is range of [0, 1].
    float GetVerticalPosition() const;
    /// @brief Changes the position of this object on a segment ie. how deep it is. This position is range of [0, 1].
    void SetVerticalPosition(float position);
    /// @brief Returns where the object is between points of its map segment. Should be in range [0, 1].
    float GetHorizontalPosition() const;
    /// @brief Changes where the object is between points of its map segment. Should be in range [0, 1].
    void SetHorizontalPosition(float position);
    /// @brief Returns the type of this entity.
    EntityType GetEntityType() const;
    /// @brief Changes the type of this entity.
    void SetEntityType(EntityType type);
    /// @brief Returns the collider used by this entity.
    const Pointer<Collider> GetCollider() const;
    /// @brief Returns the transform used by this entity.
    const Pointer<LineTransform> GetTransform() const;
    /// @brief Returns the line renderer used by this entity.
    const Pointer<LineRenderer> GetRenderer() const;
    /// @brief Calculates the world position of an entity on `segment` at `verticalPos` and `horizontalPos`.
    static Vector3 CalculatePosition(const Pointer<MapSegment> &segment, float verticalPos, float horizontalPos);
};

#include "collisions/collider.h"

#endif