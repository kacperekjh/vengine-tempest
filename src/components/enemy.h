#ifndef _TEMPEST_COMPONENTS_ENEMY_
#define _TEMPEST_COMPONENTS_ENEMY_

#include "entity.h"
#include "collisions/collider.h"

class Enemy : public Entity
{
private:
    void OnPlayerProjectileHitBase();

protected:
    void Awake() override;
    void LateUpdate() override;
    /// @brief Shoots the default enemy projectile
    void Shoot();
    /// @brief Makes the enemy immune to player projectiles if `immune` is true.
    void MakeImmune(bool immune);
    /// @brief Spawns default enemy explosion particles.
    void Explode(float radius = 1.6f);
    /// @brief Function that runs when the priority of this enemy's renderer gets updated.
    virtual void OnRenderPriorityChange(int priority);
    /// @brief Function that runs when the enemy gets hit by player's projectile.
    virtual void OnPlayerProjectileHit();

    bool defeated = false;
    bool destroyOnGameEnd = true;

public:
    static const inline std::set<EntityType> defaultColliderTypes = {EntityType::FriendlyProjectile, EntityType::Enemy, EntityType::Player};
    static const inline std::set<EntityType> immuneColliderTypes = {EntityType::Enemy, EntityType::Player};
    const unsigned long score;

    Enemy(unsigned long score = 0);

    bool IsDefeated() const;
    bool GetDestroyOnEnd() const;

    friend class Projectile;
};

#endif