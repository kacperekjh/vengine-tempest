#ifndef _TEMPEST_COMPONENTS_PROJECTILE_
#define _TEMPEST_COMPONENTS_PROJECTILE_

#include "entity.h"
#include "../direction.h"

class Projectile : public Entity
{
private:
    float speed;
    bool hostile = true;
    bool destroyOnHit = true;
    VerticalDirection direction = VerticalDirection::FromInside;
    bool destroyable = false;
    unsigned targetsHit = 0;

protected:
    void Awake() override;
    void Move();

public:
    Projectile();
    /// @brief Returns if this projectile is hostile.
    bool GetIsHostile() const;
    /// @brief Sets if this projectile is hostile.
    void SetIsHostile(bool hostile); 
    /// @brief Returns the speed of this projectile. 
    float GetSpeed() const;
    /// @brief Sets the speed of this projectile.
    void SetSpeed(float speed);
    /// @brief Returns if the projectile should be destroyed after hitting something.
    bool GetDestroyOnHit() const;
    /// @brief Changes if the projectile should be destroyed after hitting something.
    void SetDestroyOnHit(bool destroy);
    /// @brief Returns the direction this projectile moves in.
    VerticalDirection GetDirection() const;
    /// @brief Changes the direction this projectile moves in.
    void SetDirection(VerticalDirection direction);
    /// @brief Returns if this projectile can be destroyed by other projectiles.
    bool GetDestroyable() const;
    /// @brief Changes if this projectile can be destroyed by other projectiles.
    void SetDestroyable(bool destroyable);
    /// @brief Returns how many targets has this projectile already hit.
    unsigned GetTargetsHit() const;
};

#endif