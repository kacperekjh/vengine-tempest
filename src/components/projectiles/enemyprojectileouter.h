#ifndef _TEMPEST_COMPONENTS_PROJECTILES_ENEMYPROJECTILEOUTER_
#define _TEMPEST_COMPONENTS_PROJECTILES_ENEMYPROJECTILEOUTER_

#include <vengine>

class EnemyProjectileOuter : public Component
{
private:
    Pointer<LineTransform> transform = nullptr;

protected:
    void Awake() override;
    void Update() override;

public:
    static constexpr float length = 0.25f;
    static constexpr float rotationSpeed = 90_deg;
    static constexpr Color color = {255, 255, 255, 255};
    static constexpr float lineWidth = 5.f;

    void SetVertical(bool vertical);
};

#endif