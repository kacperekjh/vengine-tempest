#ifndef _TEMPEST_COMPONENTS_PROJECTILES_ENEMYPROJECTILE_
#define _TEMPEST_COMPONENTS_PROJECTILES_ENEMYPROJECTILE_

#include "../projectile.h"

class EnemyProjectile : public Projectile
{
private:
    void CreateOuter(Vector3 position, bool vertical);

protected:
    void Awake() override;
    void Update() override;
    void OnDestroy() override;
    void OnPositionChange(const Vector3 &position, const Vector3 &rotation);

public:
    static constexpr float outerRadius = 0.35f;
    static constexpr float length = 0.13f;
    static constexpr float innerRadius = 0.15f;
    static constexpr float rotationSpeed = 180_deg;
    static constexpr Color color = {255, 0, 0, 255};
    static constexpr float lineWidth = 4.f;
    static constexpr float speed = 0.35f;
};

#endif