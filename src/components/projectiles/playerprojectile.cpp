#include "playerprojectile.h"
#include "../../renderorder.h"

void PlayerProjectile::Awake()
{
    Projectile::Awake();
    Projectile::SetSpeed(speed);
    Projectile::SetIsHostile(false);
    Projectile::SetDirection(VerticalDirection::ToInside);

    // part b
    partB = ObjectManager::CreateObject(GetScene());
    partB->SetParent(GetOwner());
    partB->AddComponent<LineTransform>()->SetLines({
        {{{0, length, 0}, {0, -length, 0}}},
        {{{length, 0, 0}, {-length, 0, 0}}}
    });
    auto renderer = partB->AddComponent<LineRenderer>();
    renderer->SetColor(colorB);
    renderer->SetWidth(lineWidth);
    renderer->SetPriority(RenderOrder::Projectile1);
    renderer->SetAntialias(false);

    // part a
    constexpr float length2 = length / std::sqrt(2);
    constexpr float epsilon = 1e-2;
    GetTransform()->SetLines({
        {{{-length2, length2 + epsilon, 0}, {length2, -length2 - epsilon, 0}}},
        {{{-length2, -length2 - epsilon, 0}, {length2, length2 + epsilon, 0}}}
    });
    GetRenderer()->SetColor(colorA);
    GetRenderer()->SetWidth(lineWidth);
    GetRenderer()->SetPriority(RenderOrder::Projectile2);
}

void PlayerProjectile::Update()
{
    Move();
}

void PlayerProjectile::OnDestroy()
{
    Projectile::OnDestroy();
    partB->Destroy();
}

void PlayerProjectile::OnPositionChange(const Vector3 &position, const Vector3 &rotation)
{
    GetOwner()->position = position;
    // GetOwner()->rotation = rotation;
}