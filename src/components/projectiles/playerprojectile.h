#ifndef _TEMPEST_COMPONENTS_PROJECTILES_PLAYERPROJECTILE_
#define _TEMPEST_COMPONENTS_PROJECTILES_PLAYERPROJECTILE_

#include "../projectile.h"

class PlayerProjectile : public Projectile
{
private:
    Pointer<GameObject> partB;

protected:
    void Awake() override;
    void Update() override;
    void OnDestroy() override;
    void OnPositionChange(const Vector3 &position, const Vector3 &rotation);

public:
    static constexpr Color colorA = {223, 100, 230, 255};
    static constexpr Color colorB = {255, 255, 255, 255};
    static constexpr float speed = 1.3f;
    static constexpr float length = 0.1f;
    static constexpr float lineWidth = 4.f;
};

#endif