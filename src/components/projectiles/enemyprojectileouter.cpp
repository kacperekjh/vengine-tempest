#include "enemyprojectileouter.h"
#include "../../renderorder.h"

void EnemyProjectileOuter::Awake()
{
    transform = AddComponent<LineTransform>();
    auto renderer = AddComponent<LineRenderer>();
    renderer->SetColor(color);
    renderer->SetWidth(lineWidth);
    renderer->SetPriority(RenderOrder::Projectile1);
    renderer->SetAntialias(false);
}

void EnemyProjectileOuter::Update()
{
    GetOwner()->rotation += {0, 0, rotationSpeed * GameLoop::GetDeltaTime()};
}

void EnemyProjectileOuter::SetVertical(bool vertical)
{
    static constexpr float hLength = length / 2;
    if (vertical)
    {
        transform->SetLines({{
            {0, -hLength, 0},
            {0, hLength, 0}
        }});
    }
    else
    {
        transform->SetLines({{
            {-hLength, 0, 0},
            {hLength, 0, 0}
        }});
    }
}