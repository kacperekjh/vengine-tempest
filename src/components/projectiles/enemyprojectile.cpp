#include "enemyprojectile.h"
#include "enemyprojectileouter.h"
#include "../../renderorder.h"
#include "../../particles/explosion.h"

void EnemyProjectile::CreateOuter(Vector3 position, bool vertical)
{
    const auto &object = ObjectManager::CreateObject(GetScene());
    object->AddComponent<EnemyProjectileOuter>()->SetVertical(vertical);
    object->SetParent(GetOwner());
    object->position = position;
}

void EnemyProjectile::Awake()
{
    Projectile::Awake();
    SetSpeed(speed);
    SetDirection(VerticalDirection::FromInside);
    SetDestroyable(true);
    SetIsHostile(true);
    GetCollider()->SetWidth(innerRadius * 2);
    static constexpr float hLength = length / 2;
    GetTransform()->SetLines({
        {{{-innerRadius, -hLength, 0}, {-innerRadius, hLength, 0}}},
        {{{-hLength, innerRadius, 0}, {hLength, innerRadius, 0}}},
        {{{innerRadius, hLength, 0}, {innerRadius, -hLength, 0}}},
        {{{hLength, -innerRadius, 0}, {-hLength, -innerRadius, 0}}}
    });
    GetRenderer()->SetColor(color);
    GetRenderer()->SetWidth(lineWidth);
    GetRenderer()->SetPriority(RenderOrder::Projectile1);
    CreateOuter({-outerRadius, 0, 0}, false);
    CreateOuter({0, outerRadius, 0}, true);
    CreateOuter({outerRadius, 0, 0}, false);
    CreateOuter({0, -outerRadius, 0}, true);
}

void EnemyProjectile::Update()
{
    Move();
    GetOwner()->rotation += {0, 0, rotationSpeed * GameLoop::GetDeltaTime()};
}

void EnemyProjectile::OnDestroy()
{
    Projectile::OnDestroy();
    for (const auto &child : GetOwner()->GetChildren())
    {
        child->Destroy();
    }
    if (GetTargetsHit() == 0)
    {
        auto explosion = Explosion();
        explosion.radius = 0.5f;
        explosion.particleCount = 6;
        explosion.Explode(GetScene(), GetOwner()->GetGlobalPosition());
    }
}

void EnemyProjectile::OnPositionChange(const Vector3 &position, const Vector3 &rotation)
{
    GetOwner()->position = position;
}