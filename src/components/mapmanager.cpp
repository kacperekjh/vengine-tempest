#include "mapmanager.h"
#include "../renderorder.h"
#include <algorithm>

void MapManager::Select(const Pointer<LineRenderer> &renderer)
{
    if (!overrodeColor)
    {
        renderer->SetColor(selectedColor);
    }
}

void MapManager::Unselect(const Pointer<LineRenderer> &renderer)
{
    if (!overrodeColor)
    {
        renderer->SetColor(defaultColor);
    }
}

Pointer<LineRenderer> MapManager::CreateLine(Vector2 point)
{
    const auto &object = ObjectManager::CreateObject(GetScene());
    object->AddComponent<LineTransform>()->SetLines({{
        {point.x, point.y, 0},
        {point.x, point.y, depth}
    }});
    auto renderer = object->AddComponent<LineRenderer>();
    renderer->SetPriority(RenderOrder::Map);
    Unselect(renderer);
    return renderer;
}

const Pointer<GameObject> &MapManager::CreateRing(const Line2D &line, float depth, int priority)
{
    const auto &linePoints = line.GetPoints();
    std::vector<Vector3> points = std::vector<Vector3>(linePoints.size());
    for (size_t i = 0; i < linePoints.size(); i++)
    {
        points[i] = {linePoints[i].x, linePoints[i].y, depth};
    }
    const auto &object = ObjectManager::CreateObject(GetScene());
    object->AddComponent<LineTransform>()->SetLines(Line3D(points));
    auto renderer = object->AddComponent<LineRenderer>();
    renderer->SetColor(defaultColor);
    renderer->SetPriority(priority);
    return object;
}

void MapManager::Awake()
{
    SetInstance();
}

MapManager::~MapManager()
{
    DestroyMap();
}

const std::vector<Pointer<MapSegment>> &MapManager::CreateMap(const Line2D &line)
{
    assert(line.GetPoints().size() > 2);
    DestroyMap();
    // create segments
    const auto &points = line.GetPoints();
    closed = points[0] == points[points.size() - 1];
    segments = std::vector<Pointer<MapSegment>>(points.size() - 1, nullptr);
    lines = std::vector<Lines>(points.size() - 1);
    for (size_t i = 0; i < points.size() - 1; i++)
    {
        segments[i] = (new MapSegment())->GetPointer();
        assert(std::abs(Vector2::Distance(points[i], points[i + 1]) - MapManager::length) <= 0.02f); // bad length
        segments[i]->SetPoints(points[i], points[i + 1]);
    }
    // join segments
    for (size_t i = closed ? 0 : 1; i < segments.size(); i++)
    {
        if (i == 0)
        {
            segments[0]->previous = segments[segments.size() - 1];
            segments[segments.size() - 1]->next = segments[0];
        }
        else
        {
            segments[i - 1]->next = segments[i];
            segments[i]->previous = segments[i - 1];
        }
    }
    // create vertical lines
    if (closed)
    {
        lines[0].line1 = CreateLine(points[0]);
        lines[lines.size() - 1].line2 = lines[0].line1;
    }
    else
    {
        lines[0].line1 = CreateLine(points[0]);
        lines[lines.size() - 1].line2 = CreateLine(points[points.size() - 1]);
    }
    for (size_t i = 1; i < segments.size(); i++)
    {
        lines[i].line1 = CreateLine(points[i]);
        lines[i - 1].line2 = lines[i].line1;
    }
    // create rings
    innerRing = CreateRing(line, 0, RenderOrder::Map);
    outerRing = CreateRing(line, depth, RenderOrder::MapFar);
    return segments;
}

const std::vector<Pointer<MapSegment>> &MapManager::GetSegments() const
{
    return segments;
}

void MapManager::DestroyMap()
{
    ObjectManager::Destroy(innerRing);
    ObjectManager::Destroy(outerRing);
    for (const auto &segment : segments)
    {
        segment.Delete();
    }
    for (const auto &[line1, line2] : lines)
    {
        line1->GetOwner()->Destroy();
        line2->GetOwner()->Destroy();
    }
    segments.clear();
    lines.clear();
    selected = std::nullopt;
}

const Pointer<MapSegment> &MapManager::GetSelectedSegment() const
{
    return selected ? segments[selected.value()] : Pointer<MapSegment>::null;
}

void MapManager::SetSelectedSegment(const Pointer<MapSegment> &segment)
{
    auto it = std::find(segments.begin(), segments.end(), segment);
    if (selected)
    {
        Unselect(lines[selected.value()].line1);
        Unselect(lines[selected.value()].line2);
    }
    if (it != segments.end())
    {
        selected = std::distance(segments.begin(), it);
        Select(lines[selected.value()].line1);
        Select(lines[selected.value()].line2);
    }
    else
    {
        selected = std::nullopt;
    }
}

void MapManager::OverrideColor(Color color)
{
    for (const auto &line : lines)
    {
        line.line1->SetColor(color);
    }
    overrodeColor = true;
}

void MapManager::ClearOverrideColor()
{
    overrodeColor = false;
    for (const auto &line : lines)
    {
        line.line1->SetColor(defaultColor);
    }
    if (selected)
    {
        Select(lines[selected.value()].line1);
        Select(lines[selected.value()].line2);
    }
}

bool MapManager::IsMapClosed() const
{
    return closed;
}