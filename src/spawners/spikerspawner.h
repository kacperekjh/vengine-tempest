#ifndef _TEMPEST_SPAWNERS_SPIKERSPAWNER_
#define _TEMPEST_SPAWNERS_SPIKERSPAWNER_

#include "../enemyspawner.h"
#include "../mapsegment.h"

class SpikerSpawner : public EnemySpawner
{
private:
    std::vector<Pointer<MapSegment>> GetFreeSegments() const;

public:
    static constexpr long minStage = 2;

    bool CanSpawn() const override;
    void Spawn() const override;
};

#endif