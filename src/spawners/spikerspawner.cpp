#include "spikerspawner.h"
#include "../components/gamemanager.h"
#include "../components/mapmanager.h"
#include "../components/entitymanager.h"
#include "../components/enemies/spiker.h"
#include "../components/enemies/spike.h"
#include <algorithm>

std::vector<Pointer<MapSegment>> SpikerSpawner::GetFreeSegments() const
{
    auto freeSegments = MapManager::GetInstance()->GetSegments();
    const auto &enemies = EntityManager::GetInstance()->GetEntities(EntityType::Enemy);
    for (const auto &enemy : enemies)
    {
        bool occupies = false;
        occupies |= enemy.DynamicCast<Spiker>().GetState();
        occupies |= enemy.DynamicCast<Spike>().GetState();
        if (occupies)
        {
            auto it = std::find(freeSegments.begin(), freeSegments.end(), enemy->GetCurrentSegment());
            if (it != freeSegments.end())
            {
                freeSegments.erase(it);
            }
        }
    }
    return freeSegments;
}

bool SpikerSpawner::CanSpawn() const
{
    if (GameManager::GetInstance()->GetCurrentStage() < minStage || GetFreeSegments().size() == 0)
    {
        return false;
    }
    return true;
}

void SpikerSpawner::Spawn() const
{
    auto segments = GetFreeSegments();
    const auto &object = ObjectManager::CreateObject();
    auto spiker = object->AddComponent<Spiker>();
    spiker->SetCurrentSegment(segments[Random::GetRandomNumber<size_t>(0u, segments.size())]);
}
