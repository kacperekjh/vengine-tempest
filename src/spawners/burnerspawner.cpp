#include "burnerspawner.h"
#include "../components/gamemanager.h"
#include "../components/mapmanager.h"
#include "../components/entitymanager.h"
#include "../components/enemies/burner.h"

bool BurnerSpawner::CanSpawn() const
{
    if (!MapManager::GetInstance()->IsMapClosed() || GameManager::GetInstance()->GetCurrentStage() < minStage)
    {
        return false;
    }
    const auto &enemies = EntityManager::GetInstance()->GetEntities(EntityType::Enemy);
    for (const auto &enemy : enemies)
    {
        if (enemy.DynamicCast<Burner>().GetState())
        {
            return false;
        }
    }
    return true;
}

void BurnerSpawner::Spawn() const
{
    const auto &segments = MapManager::GetInstance()->GetSegments();
    const auto &object = ObjectManager::CreateObject();
    auto blaster = object->AddComponent<Burner>();
    blaster->SetCurrentSegment(segments[Random::GetRandomNumber<size_t>(0u, segments.size())]);
}
