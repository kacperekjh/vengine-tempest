#ifndef _TEMPEST_SPAWNERS_BASICSPAWNER_
#define _TEMPEST_SPAWNERS_BASICSPAWNER_

#include "../components/enemy.h"
#include "../components/gamemanager.h"
#include "../components/mapmanager.h"
#include "../enemyspawner.h"

template <class T>
concept ValidEnemy = std::is_convertible_v<T *, Enemy *>;

template <ValidEnemy T, unsigned MinStage = 1>
class BasicSpawner : public EnemySpawner
{
public:
    bool CanSpawn() const override;
    void Spawn() const override;
};

#include "basicspawner.hxx"

#endif