#ifndef _TEMPEST_SPAWNERS_FLIPPERSPAWNER_
#define _TEMPEST_SPAWNERS_FLIPPERSPAWNER_

#include "basicspawner.h"
#include "../components/enemies/flipper.h"

using FlipperSpawner = BasicSpawner<Flipper>;

#endif