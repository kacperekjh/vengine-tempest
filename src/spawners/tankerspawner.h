#ifndef _TEMPEST_SPAWNERS_TANKERSPAWNER_
#define _TEMPEST_SPAWNERS_TANKERSPAWNER_

#include "basicspawner.h"
#include "../components/enemies/tanker.h"

using TankerSpawner = BasicSpawner<Tanker, 3>;

#endif