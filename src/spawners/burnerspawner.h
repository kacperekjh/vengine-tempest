#ifndef _TEMPEST_SPAWNERS_BURNERSPAWNER_
#define _TEMPEST_SPAWNERS_BURNERSPAWNER_

#include "../enemyspawner.h"

class BurnerSpawner : public EnemySpawner
{
public:
    static constexpr long minStage = 3;

    bool CanSpawn() const override;
    void Spawn() const override;
};

#endif