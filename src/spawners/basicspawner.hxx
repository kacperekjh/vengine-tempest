template <ValidEnemy T, unsigned MinStage>
bool BasicSpawner<T, MinStage>::CanSpawn() const
{
    return GameManager::GetInstance()->GetCurrentStage() >= MinStage;
}

template <ValidEnemy T, unsigned MinStage>
void BasicSpawner<T, MinStage>::Spawn() const
{
    const auto object = ObjectManager::CreateObject();
    auto enemy = object->AddComponent<T>();
    const auto &segments = MapManager::GetInstance()->GetSegments();
    enemy->SetCurrentSegment(segments[Random::GetRandomNumber<size_t>(0u, segments.size())]);
}