#ifndef _TEMPEST_SPAWNERS_FUSEBALLSPAWNER_
#define _TEMPEST_SPAWNERS_FUSEBALLSPAWNER_

#include "basicspawner.h"
#include "../components/enemies/fuseball.h"

using FuseBallSpawner = BasicSpawner<FuseBall, 4>;

#endif