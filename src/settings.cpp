#include "settings.h"
#include <vengine>

bool Settings::GetFullScreen()
{
    return fullscreen;
}

void Settings::SetFullScreen(bool fullscreen)
{
    Settings::fullscreen = fullscreen;
    Renderer::SetFullscreen(fullscreen);
}
