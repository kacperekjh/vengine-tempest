#ifndef _TEMPEST_PARTICLES_EXPLOSION_
#define _TEMPEST_PARTICLES_EXPLOSION_

#include <vengine>

/// @brief Class configuring particle data for explosions of varying radius.
class Explosion
{
public:
    float timeMin = 2.f;
    float timeMax = 3.5f;
    float lineWidthMin = 9.f;
    float lineWidthMax = 10.f;
    float crossSize = 0.09f;
    Gradient<Color> colors = Gradient<Color>({
        {0.f, {255, 255, 255, 255}},
        {0.2f, {255, 223, 144, 255}},
        {0.40f, {255, 36, 0, 200}},
        {0.5f, {0, 0, 0, 130}},
        {1.f, {0, 0, 0, 0}}
    });
    float radius = 1.f;
    unsigned particleCount = 10;

    ParticleSpawnData GetSpawnData(float explosionRadius);
    ParticleVisualData GetVisualData(float explosionRadius);
    ParticleBehaviorData GetBehaviorData(float explosionRadius);
    void Explode(const Pointer<Scene> &scene, const Vector3 &position);
};

#endif