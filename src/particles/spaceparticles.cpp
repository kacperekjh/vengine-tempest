#include "spaceparticles.h"

ParticleVisualData SpaceParticles::GetVisualData()
{
    ParticleVisualData data = {};
    data.lines = {{{{0.f, -lineLength / 2, 0.f}, {0.f, lineLength / 2, 0.f}}}};
    data.lineWidthMin = lineWidthMin;
    data.lineWidthMax = lineWidthMax;
    Color colorTransparent = {color.r, color.g, color.b, 0};
    const auto gradient = Gradient<Color>({
        {0.f, colorTransparent},
        {0.1f, color},
        {0.9f, color},
        {1.f, colorTransparent}
    });
    data.colorBehavior = ColorBehaviors::Transition(gradient);
    return data;
}

ParticleBehaviorData SpaceParticles::GetBehaviorData()
{
    ParticleBehaviorData data = {};
    data.speedMin = speedMin;
    data.speedMax = speedMax;
    data.timeMin = timeMin;
    data.timeMax = timeMax;
    data.positionBehavior = MoveBehaviors::LinearMovement();
    return data;
}
