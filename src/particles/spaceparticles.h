#ifndef _TEMPEST_PARTICLES_SPACEPARTICLES_
#define _TEMPEST_PARTICLES_SPACEPARTICLES_

#include <vengine>

/// @brief Class generating base configuration for space particles.
class SpaceParticles
{
public:
    Color color = {0, 0, 255, 255};
    float lineLength = 0.025f;
    float lineWidthMin = 4;
    float lineWidthMax = 6;
    float speedMin = 0.05f;
    float speedMax = 0.1f;
    float timeMin = 10.f;
    float timeMax = 15.f;

    ParticleVisualData GetVisualData();
    ParticleBehaviorData GetBehaviorData();
};

#endif