#include "explosion.h"
#include "../renderorder.h"

ParticleSpawnData Explosion::GetSpawnData(float explosionRadius)
{
    ParticleSpawnData data;
    float spawnSize = explosionRadius * 0.1f;
    data.getPosition = SpawnBehaviors::RandomBox({-spawnSize, -spawnSize, -spawnSize}, {spawnSize, spawnSize, spawnSize});
    return data;
}

ParticleVisualData Explosion::GetVisualData(float explosionRadius)
{
    ParticleVisualData data = {};
    data.lines = {
        {{{-crossSize, crossSize, 0}, {crossSize, -crossSize, 0}}},
        {{{-crossSize, -crossSize, 0}, {crossSize, crossSize, 0}}}};
    data.lineWidthMin = lineWidthMin;
    data.lineWidthMax = lineWidthMax;
    data.colorBehavior = ColorBehaviors::Transition(colors);
    data.renderPriority = RenderOrder::Particles;
    return data;
}

ParticleBehaviorData Explosion::GetBehaviorData(float explosionRadius)
{
    ParticleBehaviorData data = {};
    data.timeMin = timeMin;
    data.timeMax = timeMax;
    data.speedMin = explosionRadius / timeMin;
    data.speedMax = explosionRadius / timeMax;
    data.positionBehavior = MoveBehaviors::LinearMovement(0.f, TimingFunctions::FadeOut);
    data.scaleBehavior = MoveBehaviors::Transition(Vector3(1, 1, 1), Vector3(0, 0, 0), TimingFunctions::FadeOut);
    return data;
}

void Explosion::Explode(const Pointer<Scene> &scene, const Vector3 &position)
{
    ParticleEmiter::EmitBurst(
        scene,
        position,
        Explosion::GetVisualData(radius),
        Explosion::GetBehaviorData(radius),
        Explosion::GetSpawnData(radius),
        particleCount);
}