#ifndef _TEMPEST_RENDERORDER_
#define _TEMPEST_RENDERORDER_

// A namespace, so it can be used like an enum class, but can be implictly converted to int.
namespace RenderOrder
{
    enum RenderOrder : int
    {
        MapFar,
        EnemyFar,
        PlayerFar,
        Map,
        Enemy,
        Player,
        Projectile1,
        Projectile2,
        Particles
    };
};

#endif