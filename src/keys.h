#ifndef _TEMPEST_KEYS_
#define _TEMPEST_KEYS_

#include <vengine>

class Keys
{
public:
    enum KeyName : unsigned
    {
        Left,
        Right,
        Shoot,
        Superzapper
    };

    /// @brief Returns the scancode for `name`.
    static SDL_Scancode Get(KeyName name);
    /// @brief Changes the scancode for `name`.
    static void Set(KeyName name, SDL_Scancode key);

    Keys() = delete;

private:
    static inline std::map<KeyName, SDL_Scancode> keys = {
        {Left, SDL_SCANCODE_LEFT},
        {Right, SDL_SCANCODE_RIGHT},
        {Shoot, SDL_SCANCODE_X},
        {Superzapper, SDL_SCANCODE_Z}
    };
};

#endif