#ifndef _TEMPEST_DIRECTION_
#define _TEMPEST_DIRECTION_

enum class HorizontalDirection
{
    CW,
    CCW
};

enum class VerticalDirection
{
    FromInside,
    ToInside
};

#endif