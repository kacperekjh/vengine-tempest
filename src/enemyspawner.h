#ifndef _TEMPEST_ENEMYSPAWNER_
#define _TEMPEST_ENEMYSPAWNER_

#include <vengine>

class EnemySpawner
{
public:
    virtual ~EnemySpawner() = default;
    virtual bool CanSpawn() const = 0;
    virtual void Spawn() const = 0;

    static void SpawnRandomEnemy();
};

#endif