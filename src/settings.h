#ifndef _TEMPEST_SETTINGS_
#define _TEMPEST_SETTINGS_

class Settings
{
private:
    static inline bool fullscreen = false;

public:
    static bool GetFullScreen();
    static void SetFullScreen(bool fullscreen);
    static inline bool fpsCounter = false;

    Settings() = delete;
};

#endif