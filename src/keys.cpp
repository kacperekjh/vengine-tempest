#include "keys.h"

SDL_Scancode Keys::Get(KeyName name)
{
    return keys.at(name);
}

void Keys::Set(KeyName name, SDL_Scancode key)
{
    keys[name] = key;
}